project( cprime )

cmake_minimum_required( VERSION 3.8 )

set( PROJECT_VERSION 4.2.0 )
set( PROJECT_VERSION_MAJOR 4 )
set( PROJECT_VERSION_MINOR 2 )
set( PROJECT_VERSION_PATCH 0 )

set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )

set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTOUIC ON )
set( CMAKE_AUTORCC ON )

add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

find_package ( Qt5Widgets REQUIRED )
find_package ( Qt5Core REQUIRED )
find_package ( Qt5Gui REQUIRED )
find_package ( Qt5Network REQUIRED )
find_package ( Qt5DBus REQUIRED )

###### CPrime Core

set ( cprime-core_HDRS
    cprime/qtlockedfile.h
    cprime/activitesmanage.h
    cprime/cenums.h
    cprime/desktopfile.h
    cprime/filefunc.h
    cprime/sortfunc.h
    cprime/systemxdg.h
    cprime/variables.h
    cprime/qtlocalpeer.h
    cprime/ccoreapplication.h
)

set ( cprime-core_SRCS
	cprime/activitesmanage.cpp
	cprime/ccoreapplication.cpp
	cprime/qtlocalpeer.cpp
	cprime/desktopfile.cpp
	cprime/filefunc.cpp
	cprime/sortfunc.cpp
	cprime/systemxdg.cpp
	cprime/variables.cpp
)

add_library ( cprime-core SHARED ${cprime-core_SRCS} ${cprime-core_HDRS} )
set_target_properties( cprime-core PROPERTIES VERSION ${PROJECT_VERSION} )
set_target_properties( cprime-core PROPERTIES SOVERSION ${PROJECT_VERSION} )
set_target_properties( cprime-core PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR_MINOR} )
set_target_properties( cprime-core PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR} )
target_link_libraries ( cprime-core  Qt5::Core Qt5::Network )

###### CPrime Gui

set ( cprime-gui_SRCS
	cprime/cguiapplication.cpp
)

set ( cprime-gui_MOCS
	cprime/cguiapplication.h
)

add_library ( cprime-gui SHARED ${cprime-gui_SRCS} ${cprime-gui_MOCS} )
set_target_properties( cprime-gui PROPERTIES VERSION ${PROJECT_VERSION} )
set_target_properties( cprime-gui PROPERTIES SOVERSION ${PROJECT_VERSION} )
set_target_properties( cprime-gui PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR_MINOR} )
set_target_properties( cprime-gui PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR} )
target_link_libraries ( cprime-gui  Qt5::Core Qt5::Gui cprime-core )

###### CPrime Widgets

set ( cprime-widgets_HDRS
	cprime/appopenfunc.h
	cprime/cprime.h
	cprime/pinmanage.h
	cprime/themefunc.h
	cprime/trashmanager.h
    cprime/capplication.h
	cprime/applicationdialog.h
	cprime/cplugininterface.h
	cprime/ioprocesses.h
	cprime/messageengine.h
	cprime/pinit.h
	cprime/shareit.h
)

set ( cprime-widgets_SRCS
	cprime/capplication.cpp
	cprime/applicationdialog.cpp
	cprime/appopenfunc.cpp
	cprime/ioprocesses.cpp
	cprime/messageengine.cpp
	cprime/pinit.cpp
	cprime/pinmanage.cpp
	cprime/shareit.cpp
	cprime/themefunc.cpp
	cprime/trashmanager.cpp
)

set ( cprime-widgets_UIS
	cprime/pinit.ui
	cprime/shareit.ui
)

add_library ( cprime-widgets SHARED ${cprime-widgets_SRCS} ${cprime-widgets_UIS} ${cprime-widgets_HDRS} )
set_target_properties( cprime-widgets PROPERTIES VERSION ${PROJECT_VERSION} )
set_target_properties( cprime-widgets PROPERTIES SOVERSION ${PROJECT_VERSION} )
set_target_properties( cprime-widgets PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR_MINOR} )
set_target_properties( cprime-widgets PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR} )
target_link_libraries ( cprime-widgets  Qt5::Core Qt5::Gui Qt5::Widgets Qt5::DBus cprime-core )

set( CPRIME_HEADERS
	cprime/activitesmanage.h
	cprime/capplication.h
	cprime/ccoreapplication.h
	cprime/cguiapplication.h
	cprime/applicationdialog.h
	cprime/appopenfunc.h
	cprime/cenums.h
	cprime/cplugininterface.h
	cprime/cprime.h
	cprime/desktopfile.h
	cprime/filefunc.h
	cprime/ioprocesses.h
	cprime/messageengine.h
	cprime/pinit.h
	cprime/pinmanage.h
	cprime/shareit.h
	cprime/sortfunc.h
	cprime/systemxdg.h
	cprime/themefunc.h
	cprime/trashmanager.h
	cprime/variables.h
	libcprime_global.h
)

configure_file( pkgconfig/cprime-core.pc.in cprime-core.pc @ONLY )
configure_file( pkgconfig/cprime-gui.pc.in cprime-gui.pc @ONLY )
configure_file( pkgconfig/cprime-widgets.pc.in cprime-widgets.pc @ONLY )

install( TARGETS cprime-core cprime-gui cprime-widgets EXPORT LIBRARY )
install( FILES ${CPRIME_HEADERS} DESTINATION include/cprime/ )

install(
	FILES
		${CMAKE_CURRENT_BINARY_DIR}/cprime-core.pc
		${CMAKE_CURRENT_BINARY_DIR}/cprime-gui.pc
		${CMAKE_CURRENT_BINARY_DIR}/cprime-widgets.pc
	DESTINATION
		share/pkgconfig
)
