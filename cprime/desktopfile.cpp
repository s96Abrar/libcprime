/*
	*
	* This file is a part of Libcprime.
	* Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, vsit http://www.gnu.org/licenses/.
	*
*/

#include <QSettings>
#include <QProcess>
#include <QDir>
#include <QUrl>

#include "filefunc.h"
#include "systemxdg.h"
#include "desktopfile.h"

using namespace CPrime;

DesktopFile::DesktopFile(const QString &filename)
{
	if (filename.isEmpty()) {
		return;
	}

	mFileUrl = SystemXdgMime::instance()->desktopPathForName(filename);
	mDesktopName = CPrime::FileUtils::baseName(filename);

	if (mFileUrl.startsWith("/usr/share/applications")) {
		mRank = 1;
	}

	else if (mFileUrl.startsWith("/usr/local/share/applications")) {
		mRank = 2;
	}

	else if (mFileUrl.startsWith(QDir::home().filePath(".local/share/applications"))) {
		mRank = 3;
	}

	QSettings s(mFileUrl, QSettings::NativeFormat);
	s.beginGroup("Desktop Entry");

	mName = s.value("Name").toString();
	mGenericName = s.value("GenericName", mName).toString();
	mDescription = s.value("Description", "").toString();
	mCommand = s.value("Exec").toString();
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
	mExec = s.value("TryExec", mCommand.split(" ", Qt::SkipEmptyParts).value(0)).toString();
#else
	mExec = s.value("TryExec", mCommand.split(" ", QString::SkipEmptyParts).value(0)).toString();
#endif

	if (not CPrime::FileUtils::exists(mExec)) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
		Q_FOREACH (const QString &path, QString::fromLocal8Bit(qgetenv("PATH")).split(":", Qt::SkipEmptyParts)) {
#else
		Q_FOREACH (const QString &path, QString::fromLocal8Bit(qgetenv("PATH")).split(":", QString::SkipEmptyParts)) {
#endif
			if (CPrime::FileUtils::exists(path + "/" + mCommand)) {
				mExec = path + "/" + mCommand;
				break;
			}
		}
	}

	mIcon = s.value("Icon").toString();

	QStringList args = mCommand.split(" ");

	Q_FOREACH (const QString &arg, args) {
		if (arg == "%f" or arg == "%u") {
			mMultiArgs = false;
			mTakesArgs = true;
			mParsedArgs << "<#COREARG-FILE#>";
		}

		else if (arg == "%F" or arg == "%U") {
			mMultiArgs = true;
			mTakesArgs = true;
			mParsedArgs << "<#COREARG-FILES#>";
		}

		else if (arg == "%i") {
			if (!mIcon.isEmpty()) {
				mParsedArgs << "--icon" << mIcon;
			}
		}

		else if (arg == "%c") {
			mParsedArgs << mName;
		}

		else if (arg == "%k") {
			mParsedArgs << QUrl(mFileUrl).toLocalFile();
		}

		else {
			mParsedArgs << arg;
		}
	}

	QRegExp mimeRx("^MimeType=([.]+)$");

	QFile desktop(mFileUrl);
	desktop.open(QFile::ReadOnly);
	QStringList lines = QString::fromLocal8Bit(desktop.readAll()).split("\n");

	Q_FOREACH (QString line, lines) {
		if (line.startsWith("MimeType=")) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
			mMimeTypes = line.replace("MimeType=", "").split(";", Qt::SkipEmptyParts);
#else
			mMimeTypes = line.replace("MimeType=", "").split(";", QString::SkipEmptyParts);
#endif
		}

		if (line.startsWith("Categories=")) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
			mCategories = line.replace("Categories=", "").split(";", Qt::SkipEmptyParts);
#else
			mCategories = line.replace("Categories=", "").split(";", QString::SkipEmptyParts);
#endif
			for (QString &category : mCategories) {
				if (category.startsWith("X-")) {
					category = category.remove(0, 2);
				}
			}
		}
	}

	desktop.close();

	mVisible = not s.value("NoDisplay", false).toBool();
	mRunInTerminal = s.value("Terminal", false).toBool();

	if (s.value("Type").toString() == "Application") {
		mType = Application;
	}

	else if (s.value("Type").toString() == "Link") {
		mType = Link;
	}

	else if (s.value("Type").toString() == "Directory") {
		mType = Directory;
	}

	if (mName.count() and mCommand.count()) {
		mValid = true;
	}
}

bool DesktopFile::startApplication()
{
	if (not mValid) {
		return false;
	}

	QProcess proc;
	return proc.startDetached(mExec, QStringList());
}

bool DesktopFile::startApplicationWithArgs(QStringList args)
{
	if (not mValid) {
		return false;
	}

	QProcess proc;

	QStringList execList = parsedExec();
	QString exec = execList.takeFirst();

	if (not args.count()) {

		execList.removeAll("<#COREARG-FILES#>");
		execList.removeAll("<#COREARG-FILE#>");

		return QProcess::startDetached(exec, execList);
	}

	QStringList argList;

	if (mTakesArgs) {
		if (mMultiArgs) {
			Q_FOREACH (const QString &exeArg, execList) {
				if (exeArg == "<#COREARG-FILES#>") {
					if (args.count()) {
						argList << args;
					}
				}

				else {
					argList << exeArg;
				}
			}
		}

		else {
			int idx = execList.indexOf("<#COREARG-FILE#>");
			argList << execList;
			argList.removeAt(idx);

			if (args.count()) {
				argList.insert(idx, args.takeAt(0));
				argList << args;
			}
		}
	}

	else {
		argList << execList;

		if (args.count()) {
			argList << args;
		}
	}

	return QProcess::startDetached(exec, argList);
}

QString DesktopFile::desktopName() const
{
	return mDesktopName;
}

QString DesktopFile::name() const
{
	return mName;
}

QString DesktopFile::genericName() const
{
	return mGenericName;
}

QString DesktopFile::description() const
{
	return mDescription;
}

QString DesktopFile::exec() const
{
	return mExec;
}

QString DesktopFile::command() const
{
	return mCommand;
}

QString DesktopFile::icon() const
{
	return mIcon;
}

QStringList DesktopFile::mimeTypes() const
{
	return mMimeTypes;
}

QStringList DesktopFile::categories() const
{
	return mCategories;
}

QStringList DesktopFile::parsedExec() const
{
	return mParsedArgs;
}

int DesktopFile::type() const
{
	return mType;
}

int DesktopFile::rank() const
{
	return mRank;
}

bool DesktopFile::visible() const
{
	return mVisible;
}

bool DesktopFile::runInTerminal() const
{
	return mRunInTerminal;
}

bool DesktopFile::isValid() const
{
	return mValid;
}

QString DesktopFile::desktopFileUrl() const
{
	return mFileUrl;
}

uint qHash(DesktopFile &app)
{
	QString hashString;
	hashString += app.name();
	hashString += app.genericName();
	hashString += app.description();
	hashString += app.command();
	hashString += app.icon();
	hashString += app.mimeTypes().join(" ");
	hashString += app.categories().join(" ");

	return qChecksum(hashString.toLocal8Bit().data(), hashString.count());
}
