/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QApplication>
#include <QFileInfo>
#include <QFile>
#include <QWidget>
#include <QIcon>
#include <QStyle>
#include <QPainter>
#include <QDir>
#include <math.h>
#include <QScreen>
#include <QProcess>

#include "systemxdg.h"
#include "filefunc.h"
#include "variables.h"
#include "themefunc.h"

/*
 * Extract Icon from *.desktop file.
 */
QIcon CPrime::ThemeFunc::getAppIcon(const QString &appName)
{
	/*
		* We should not struggle so hard to search for the icons. Either it will be an absolute path
		* or a name. So our first attempt should be just QIcon::fromTheme( icoStr, QIcon( icoStr ) )
		* This will cover the standard icon specifications. Second attempt will be just see if
		* @icoStr exists in /usr/share/pixmaps. If not, return
		* QIcon::fromTheme( "application-x-executable" )
	*/

	DesktopFile df = SystemXdgMime::instance()->desktopForName( appName );
	if ( df.isValid() ) {
		QString icoStr = df.icon();

		/* First attempt */
		if ( CPrime::FileUtils::exists( icoStr ) or QIcon::hasThemeIcon( icoStr ) )
			return QIcon::fromTheme( icoStr, QIcon( icoStr ) );

		/* Second attempt */
		if ( CPrime::FileUtils::exists( "/usr/share/pixmaps/" + icoStr ) )
			return QIcon( "/usr/share/pixmaps/" + icoStr );
	}

	/* Fallback */
	qDebug() << appName << "Using fallback icon: application-x-executable";
	return QIcon::fromTheme( "application-x-executable" );
}

/*
 * Extract Icon from mime database.
 */
QIcon CPrime::ThemeFunc::getFileIcon(const QString &filePath)
{
    QFileInfo info(filePath);

    if (filePath.count() == 0 || !info.exists()) {
        return QApplication::style()->standardIcon(QStyle::SP_FileIcon);
    }

    QIcon icon;
    QMimeDatabase mimeDbInstance;
    QMimeType mimeType;

    mimeType = mimeDbInstance.mimeTypeForFile(filePath);
    icon = QIcon::fromTheme(mimeType.iconName());

    if (icon.isNull()) {
        return QApplication::style()->standardIcon(QStyle::SP_FileIcon);
    }

    return icon;
}

QIcon CPrime::ThemeFunc::themeIcon( QString name1, QString name2, QString stock )
{
    if ( QIcon::hasThemeIcon( name1 ) )
        return QIcon::fromTheme( name1, QIcon( stock ) );

    else if ( QIcon::hasThemeIcon( name2 ) )
        return QIcon::fromTheme( name2, QIcon( stock ) );

    else
        return QIcon( stock );
}

QIcon CPrime::ThemeFunc::resizeIcon(QIcon originalIcon, QSize containerSize)
{
    QIcon resized = originalIcon;

    if (!resized.pixmap(48).width()) {
        resized = QIcon::fromTheme("application-x-executable");
    } else {
        QSize iSize = containerSize;
        QPixmap iPix = resized.pixmap(iSize);
        QSize actualSize = iPix.size();
        if ((actualSize.width() < iSize.width()) || actualSize.height() < iSize.height()) {
            QPixmap pix(iSize);
            pix.fill(Qt::transparent);

            QPainter painter(&pix);
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);

            QRect iconRect(QPoint((iSize.width() - actualSize.width()) / 2, (iSize.height() - actualSize.height()) / 2), actualSize);
            painter.drawPixmap(iconRect, iPix);

            resized = QIcon(pix);
        }
    }

    return resized;
}


int CPrime::ThemeFunc::getFormFactor() {

    QSizeF screenSize = QGuiApplication::primaryScreen()->physicalSize();
    double diag = sqrt( pow( screenSize.width(), 2 ) + pow( screenSize.height(), 2 ) ) / 25.4;

    //- Deady large screen mobiles
    if ( diag <= 6.5 )
        return 2;

    //- Tablets and small netbooks
    else if ( diag <= 11 )
        return 1;

    //- Typical desktops
    else
        return 3;

    /* Default value */
    return 0;
}

bool CPrime::ThemeFunc:: getTouchMode()
{
    QProcess proc;
    proc.start("udevadm", QStringList() << "info" << "--export-db");
    proc.waitForFinished(-1);

    QString output = QString::fromLocal8Bit(proc.readAllStandardOutput() + '\n' + proc.readAllStandardError());

    if (output.contains("ID_INPUT_TOUCHSCREEN=1")) {
        return true;
    }

    return false;
}
