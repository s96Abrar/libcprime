/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QDir>
#include <QStandardPaths>

#include "variables.h"

QString CPrime::Variables::CC_PinsFilePath()
{
	return QDir(CC_Library_ConfigDir()).filePath(CC_PinsFile());
}

QString CPrime::Variables::CC_DefaultAppListFilePath()
{
	return QDir(CC_System_ConfigDir()).filePath(CC_DefaultAppListFile());
}

QString CPrime::Variables::CC_ActivitiesFilePath()
{
	return QDir(CC_Library_ConfigDir()).filePath(CC_ActivitesFile());
}

QString CPrime::Variables::CC_Home_TrashDir()
{
	return QDir::home().filePath(".local/share/Trash");
}

QString CPrime::Variables::CC_Library_ConfigDir()
{
	return QDir(CC_System_ConfigDir()).filePath("cubocore/");
}

QString CPrime::Variables::CC_System_ConfigDir()
{
	return QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/";
}

QString CPrime::Variables::CC_Widgets_ConfigDir()
{
	return QDir(CC_Library_ConfigDir()).filePath("plugins/");
}
