/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QIcon>
#include <QDirIterator>
#include <QStorageInfo>
#include <QSettings>
#include <QInputDialog>
#include <QLineEdit>

#include <fcntl.h>
#include <unistd.h>

#include "variables.h"
#include "trashmanager.h"
#include "filefunc.h"
#include "ioprocesses.h"
#include "messageengine.h"


QString CPrime::TrashManager::homePartition = QStorageInfo( QDir::homePath() ).rootPath();

/*
	*
	* freedesktop.org says, check if home and path are on the same partition. The $XDG_DATA_DIR/.Trash is the trash location.
	*
	* If not, check $mountpoint/.Trash/ if it exists, then ( create and ) use $mountpoint/.Trash/$uid
	*	* $mountpoint/.Trash/ should have sticky bit set ( S_ISVTX ), and should not be a symbolic link
	*	* readLink( $mountpoint/.Trash/ ) == $mountpoint/.Trash/
	* Otherwise, use $mountpoint/.Trash-$uid
	*
*/

QString CPrime::TrashManager::trashLocation( const QString &path )
{
	/* Check if $HOME and @path are on the same partition, and path begins with $HOME */
	if ( ( homePartition == QStorageInfo( path ).rootPath() ) and path.startsWith( QDir::homePath() ) ) {

		// Same partition, ensure all the paths exist
		QDir::home().mkpath( ".local/share/Trash/" );
		QDir::home().mkpath( ".local/share/Trash/files/" );
		QDir::home().mkpath( ".local/share/Trash/info/" );

		QFile::setPermissions( QDir::home().filePath( "/.local/share/Trash" ), QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );
		QFile::setPermissions( QDir::home().filePath( "/.local/share/Trash/files" ), QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );
		QFile::setPermissions( QDir::home().filePath( "/.local/share/Trash/info" ), QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );

		return QDir::home().filePath( ".local/share/Trash" );
	}

	else {
		QString mountPoint = QStorageInfo( path ).rootPath();

		/* If the mount point does not exist, or we have read write issues return a NULL string */
		if ( access( mountPoint.toLocal8Bit().data(), R_OK | W_OK | X_OK ) )
			return QString();

		/* If $MNTPT/.Trash/$UID is present, and accessible with right permissions */
		/* We blindly try to make $MTPT/.Trash/$uid/files, $MTPT/.Trash/$uid/info */
		if( access( ( mountPoint + "/.Trash/" + QString::number( getuid() ) ).toLocal8Bit().data(), R_OK | W_OK | X_OK ) == 0 ) {
			QDir( mountPoint ).mkpath( QString( ".Trash/%1/files/" ).arg( getuid() ) );
			QDir( mountPoint ).mkpath( QString( ".Trash/%1/info/" ).arg( getuid() ) );

			/* Check if the any one above folders exist, say $MTPT/.Trash-$uid/files */
			if( access( ( mountPoint + "/.Trash/" + QString::number( getuid() ) + "/files/" ).toLocal8Bit().data(), R_OK | W_OK | X_OK ) == 0 )
				return mountPoint + "/.Trash/" + QString::number( getuid() );
		}

		/* Otherwise we create $MNTPT/.Trash-$UID */
		QDir( mountPoint ).mkpath( QString( ".Trash-%1/" ).arg( getuid() ) );
		QDir( mountPoint ).mkpath( QString( ".Trash-%1/files/" ).arg( getuid() ) );
		QDir( mountPoint ).mkpath( QString( ".Trash-%1/info/" ).arg( getuid() ) );
		QFile::setPermissions( mountPoint + "/.Trash-" + QString::number( getuid() ), QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );

		/* Check if the any one above folders exist, say $MTPT/.Trash-$uid/files */
		if( access( ( mountPoint + "/.Trash-" + QString::number( getuid() ) + "/files/" ).toLocal8Bit().data(), R_OK | W_OK | X_OK ) == 0 )
			return mountPoint + "/.Trash-" + QString::number( getuid() );

		return QString();
	}
}

bool CPrime::TrashManager::moveToTrash( const QStringList &filePaths )
{
	QStringList failedList;
	failedList.clear();
	int count = 0;
    int done = 0;

	// Is there many files to trash
	bool isMany = ( filePaths.count() > 1 ) ? true : false;
	bool yesToAll = false;

	Q_FOREACH(const QString &filePath, filePaths ) {
		QFileInfo file( filePath );

		// Check whether the file size is bigger than 1GB
		if ( file.size() >= ( 1024 * 1024 * 1024 ) ) {
			// Ask to remove the big file( 1GB )
			QString tmpMess(
				"<p>The file %1 size is 1 GB or larger in size. Please consider deleting it instead of moving to trash.</p><p>Do you want to delete it?</p>"
			 );

            int reply = QMessageBox::question( nullptr, "Trash - Warning!", tmpMess.arg( file.fileName() ), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes );

			if ( reply == QMessageBox::Yes ) {
				QFile::remove( file.filePath() );
                done++;
				continue;
			}
		}

		if ( !yesToAll ) {
			QString tmpMess = QString( "Do you want to send <b>%1</b> from <tt>%2</tt> to trash?" ).arg( file.fileName() ).arg( file.path() );

			QMessageBox::StandardButtons buttons;
			buttons |= QMessageBox::Yes;
			if ( isMany )
				buttons |= QMessageBox::YesToAll;

			buttons |= QMessageBox::No;

            int reply = QMessageBox::question( nullptr, "Trash - Warning!", tmpMess, buttons, QMessageBox::No );

			if ( reply == QMessageBox::YesToAll )
				yesToAll = true;

			else if ( reply == QMessageBox::No )
				continue;
		}

		QString trashLoc = trashLocation( file.filePath() );

		if ( !trashLoc.count() ) {
			// Failed to move to trash
			failedList[ count++ ] = file.filePath();
			continue;
		}

		//QDir trash( trashLoc );
		QString newPath = trashLoc + "/files/" + file.fileName();
		QString delTime = QDateTime::currentDateTime().toString( "yyyy-MM-ddThh:mm:ss" );

		// If it exists, add a date time to it to make it unique
		if ( QFileInfo( newPath ).exists() )
			newPath += delTime;

		/* Try trashing it. If it fails, intimate the user */
		if ( not QFile::rename( file.filePath(), newPath ) ) {

			failedList[count++] = file.filePath();
		}

		/* If it succeeds, we write the meta data */
		else {
			QFile metadata( trashLoc + "/info/" + CPrime::FileUtils::baseName( newPath ) + ".trashinfo" );
			metadata.open( QIODevice::WriteOnly );
			metadata.write(
				QString(
					"[Trash Info]\n"
					"Path=%1\n"
					"DeletionDate=%2\n"
				).arg( file.filePath() ).arg( delTime ).toLocal8Bit()
			);
			metadata.close();
            done++;
		}
	}

    bool ret = true;
    QString msg = "";
    if (count) {
        msg = "Some items could not be deleted. Please check if you have the right permissions, and try again.";
        ret = false;
    } else {
        if (done) {
            msg = "Trashing of the selected items is complete.";
            ret = true;
        } else {
            msg = "Nothing trashed.";
            ret = false;
        }
    }

	CPrime::MessageEngine::messageEngine("user-trash", "Trash", "Warning!!!", msg);

    return ret;//( count ? false : true );
}

/* Since we support multiple trash locations, use absolute trash paths: ~/.local/share/Trash/files/path */
void CPrime::TrashManager::restoreFromTrash( const QStringList &filePaths )
{
	int failed = 0;
	int response = -1;

	Q_FOREACH (const QString &fileToRestore, filePaths ) {
		QDir trash = QDir( trashLocation( fileToRestore ) );

		// Read trashinfo
		QSettings trashInfo( trash.path() + "/info/" + CPrime::FileUtils::baseName( fileToRestore ) + ".trashinfo", QSettings::IniFormat );
		if ( trashInfo.contains( "Trash Info/Path" ) ) {
			QString origPath = trashInfo.value( "Trash Info/Path" ).toString();

			if ( CPrime::FileUtils::exists( origPath ) ) {

				QString origLoc = CPrime::FileUtils::dirName( origPath );
				QString origName = CPrime::FileUtils::baseName( origPath );

				/* If the user had previously said yes or no, then ask again */
				if ( response & ( QMessageBox::Yes | QMessageBox::No | QMessageBox::Ignore ) ) {
					response = showMessage(
						QMessageBox::Question,
						"Replace existing file?",
						QString(
							"<p>The file <b><tt>%1</tt></b> you are trying to restore from trash exists in </p>"
							"<p><center><b><tt>%2</tt></b></center></p>"
							"<p>Do you want to replace it?</p>"
							"<tt>[Yes]</tt> - Restore and replace<br>"
							"<tt>[Yes to All]</tt> - Restore and replace all existing files<br>"
							"<tt>[No]</tt> - Restore and keep both files<br>"
							"<tt>[No to All]</tt> - Restore and keep all existing files<br>"
							"<tt>[Ignore]</tt> - Do not restore<br>"
						).arg( origName ).arg( origLoc ),
						QMessageBox::No | QMessageBox::NoToAll | QMessageBox::Yes | QMessageBox::YesToAll | QMessageBox::Ignore
					);
				}

				/* If the user had said yes/yes to all, delete the existing file and then continue */
				if ( response & ( QMessageBox::Yes | QMessageBox::YesToAll ) ) {
					if ( CPrime::FileUtils::isDir( origPath ) )
						CPrime::FileUtils::removeDir( origPath );

					else
						QFile::remove( origPath );
				}

				/* If the user had said no/no to all, ask for a new name and then continue */
				else if ( response & ( QMessageBox::No | QMessageBox::NoToAll ) ) {
					bool ok = false;
					while ( CPrime::FileUtils::exists( origPath ) ) {
						QString fn = QInputDialog::getText(
							nullptr,
							"Input new file name",QString(
								"<p>The file <b><tt>%1</tt></b> already exists in</p>"
								"<p><center><b><tt>%2</tt></b></center></p>"
								"Please enter a new name:"
							).arg( origName ).arg( origLoc ),
							QLineEdit::Normal,
							origName,
							&ok
						);

						if ( not ok )
							break;

						origPath = origLoc + fn;
					}

					/* If the user does not provide a new name */
					if ( not ok )
						continue;
				}

				/* Ignore the restoration of this file */
				else {
					failed++;
					continue;
				}
			}

			/* Failed to move it to original path */
			if ( not QFile::rename( fileToRestore, origPath ) )
				failed++;

			/* Success! Remove the info file */
			else
				QFile::remove( trashInfo.fileName() );
		}

		else {

			qDebug() << "Problematic info file:" << trashInfo.fileName();
			failed++;
		}
	}

	if ( failed )
		CPrime::MessageEngine::messageEngine("dialog-warning", "Trash", "Warning!!!", QString( "Failed to restore <b>%1</b> node %2." ).arg( failed ).arg( failed == 1 ? "" : "s" ));
}

bool CPrime::TrashManager::deleteFileTotally( const QStringList &filePaths, bool fromTrash )
{
	QStringList failedList;
	failedList.clear();
	int count = 0;

	// Is there many files to trash
	bool isMany = ( filePaths.count() > 1 ) ? true : false;
	bool yesToAll = false;

	Q_FOREACH(const QString &filePath, filePaths ) {
		QFileInfo file( filePath );

		QString trashLoc = TrashManager::trashLocation( filePath );

		// Check whether the directory is writable
		if ( not CPrime::FileUtils::isWritable( CPrime::FileUtils::dirName( filePath ) ) ) {
			failedList[count++] = filePath;
			continue;
		}

		// Check whether the file is link
		if ( file.isSymLink() ) {
			if ( !QFile::remove( filePath ) ) {
				failedList[count++] = filePath;
				continue;
			}
		}

		else {
			// Check whether to show the message box
			if ( !yesToAll ) {
				QString tmpMess = "Are you sure you want to delete <p><b>" + filePath + "</b>?";

				QMessageBox::StandardButtons buttons;
				buttons |= QMessageBox::No;
				if ( isMany )
					buttons |= QMessageBox::YesToAll;

				buttons |= QMessageBox::Yes;

				int reply = showMessage( QMessageBox::Question, "Careful", tmpMess, buttons );
				if ( reply == QMessageBox::YesToAll )
					yesToAll = true;

				else if ( reply == QMessageBox::No )
					continue;
			}

			// Iterate over the file path for more files inside that path
			QDirIterator it( filePath, QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::Subdirectories );
			QStringList children;

			while ( it.hasNext() ) {
				children.prepend( it.next() );
			}

			children.append( filePath );
			children.removeDuplicates();

			Q_FOREACH (const QFileInfo &info, children) {
				if ( info.isDir() ) {
					if ( !QDir().rmdir( info.filePath() ) ) {
						failedList[count++] = filePath;
					}
				}

				else {
					if ( !QFile::remove( info.filePath() ) ) {
						failedList[count++] = filePath;
					}
				}
			}
			// ========

			// Check is delete done for emptying trash
			if ( fromTrash ) {
				// Delete trash info
				QFile info( trashLoc + "/info/" + CPrime::FileUtils::baseName( filePath ) + ".trashinfo" );
				if ( info.exists() ) {
					info.remove();
				}

				else {
					qDebug() << "Failed to remove info:" << info.fileName();
				}
			}
		}

		if (!yesToAll)
			CPrime::MessageEngine::messageEngine("dialog-information", "Trash", "Info", "File Deleted" );
	}

	if (yesToAll)
		CPrime::MessageEngine::messageEngine("dialog-information", "Trash", "Info", "File Deleted" );

	bool ok = true;
	if ( count ) {
		ok = false;
		QString msg = "Could not delete some items...\nDo you have the permissions?";
		CPrime::MessageEngine::messageEngine("dialog-warning", "Trash", "Warning!!!", msg );
	} else {
		qDebug() << "Finised";
	}

	return ok;
}

int CPrime::TrashManager::showMessage(QMessageBox::Icon icon, const QString &title, const QString &message, QMessageBox::StandardButtons buttons )
{
	QMessageBox m( icon, title, message, buttons );
    m.setWindowIcon( QIcon::fromTheme("user-trash"));
	return m.exec();
}
