/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QObject>
#include <QDateTime>


namespace CPrime {

    // Related to SystemDefaultApps
    enum DefaultAppCategory {
        FileManager    = 0x0000,
        MetadataViewer = 0x0001,
        SearchApp      = 0x0002,
        ImageEditor    = 0x0003,
        Terminal       = 0x0004,
        BatchRenamer   = 0x0005
    };

    // Related to SortFunc
    enum SortOrder {
        Ascending    = 0x0000,
        Descending   = 0x0001
    };

    // Related to FileUtils
    enum FolderSetup {
        ConfigFolder       = 0x0000,
        SystemTrashFolder  = 0x0001,
        WidgetConfigFolder = 0x0002
    };

    // Related to IO porcess
    enum IOProcessType {
        Copy = 0x5CF670,				// Copy file/dir
        Move,							// Move file/dir
    };

    // Related to IO porcess
    enum IOProcessState  {
        NotStarted = 0x7A242A,			// We are yet to begin doing anything with this (for delayed start, etc)
        Starting,						// Listing the sources
        Started,						// Process is on going
        Paused,							// Process is paused
        Canceled,						// Process was cancelled
        Completed						// Process is complete (with/without errors)
    };

    // Related to IO porcess
    typedef struct IOProcess_t {
        /* The source directory */
        QString sourceDir;

        /* The target directory */
        QString targetDir;

        /* Total bytes to be copied */
        quint64 totalBytes;

        /* Total bytes already copied */
        quint64 totalBytesCopied;

        /* Current file name */
        QString currentFile;

        /* Current file size */
        quint64 fileBytes;

        /* Current file bytes already copied */
        quint64 fileBytesCopied;

        /* When did this process start */
        QDateTime startTime;

        /* Text to be displayed with the progress bar */
        QString progressText;

        /* Type: Copy, Move, Delete, Trash */
        CPrime::IOProcessType type;

        /* State: Starting, Started, Paused, Canceled, */
        CPrime::IOProcessState state;
    } IOProcess;
}
