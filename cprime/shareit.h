/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include <QDialog>
#include <QListWidget>

#include "cplugininterface.h"
#include "systemxdg.h"
#include "libcprime_global.h"


class QListWidgetItem;

namespace Ui {
    class ShareIT;
}

class LIBCPRIMESHARED_EXPORT ShareIT : public QDialog {

    Q_OBJECT

public:
    enum Type {
        Plugins = 1,
        Apps = 2,
        Folders = 3,
        Actions = 4
    };
    Q_ENUM(Type)

    /**
     * @brief Call add shareit dialog from any apps by giving the path to share it.
     * @param files : Path which needs to be pinned.
     * @param iconSize : Set icon size of the list in the dialog.
     */
	explicit ShareIT(const QStringList &files = QStringList(),
					 const QSize &iconSize = QSize(),
					 QWidget *parent = nullptr);

    ~ShareIT();

private slots:
    void folders_itemActivated(QListWidgetItem *item);
    void apps_itemActivated(QListWidgetItem *item);
    void plugin_itemActivated(QListWidgetItem *item);
    void action_itemActivated(QListWidgetItem *item);
    void copyToClipboard(QListWidgetItem *item);
    void openFileLocation();

private:
    Ui::ShareIT *ui;
    QListWidget *pluginWidget, *otherOptions;
    QStringList mFiles;
    QList<ShareItInterface *> plugins;
	CPrime::SystemXdgMime *mimeHandler;
    QString shareSelection;
	QWidget *mParent;
	QSize m_iconSize;

    void populateFolders();
    QString toDriveFolder(QString driveMountPoint);
    void populatePlugins();
    void itemSelected(QListWidgetItem *);
    void openFileWith();
    void moveToTrash();
    void populateActions();
    void populateRelatedApps();
};
