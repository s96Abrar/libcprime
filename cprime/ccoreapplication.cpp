/*
	*
	* This file is a part of PaperSessionManager.
	* PaperSessionManager is the Session Manager for PaperDesktop
	* Copyright 2020 CuboCore Group
	*

	*
	* This file is derived from QSingleApplication, originally written
	* as a part of Qt Solutions. For license read DesQApplication.cpp
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "ccoreapplication.h"
#include "qtlocalpeer.h"

namespace CPrime {

void CCoreApplication::sysInit( const QString &appId ) {

    peer = new QtLocalPeer( this, appId );
    connect( peer, SIGNAL( messageReceived( const QString& ) ), SIGNAL( messageReceived( const QString& ) ) );
};

CCoreApplication::CCoreApplication( int &argc, char **argv ) : QCoreApplication( argc, argv ) {

    sysInit();
};

CCoreApplication::CCoreApplication( const QString &appId, int &argc, char **argv ) : QCoreApplication( argc, argv ) {

    sysInit( appId );
};

bool CCoreApplication::isRunning() {

    return peer->isClient();
};

bool CCoreApplication::sendMessage( const QString &message, int timeout ) {

    return peer->sendMessage( message, timeout );
};

QString CCoreApplication::id() const {

    return peer->applicationId();
};

void CCoreApplication::disconnect() {

	peer->shutdown();
};

}
