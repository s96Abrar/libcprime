/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QProgressBar>
#include <QPushButton>
#include <QLabel>
#include <QStyleFactory>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCloseEvent>
#include <QStorageInfo>
#include <QApplication>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>

#include "filefunc.h"
#include "messageengine.h"
#include "ioprocesses.h"


CIOProcess::CIOProcess(QStringList sources, CPrime::IOProcess *progress, QWidget *parent ) : QThread( parent ) {

	mParent = qobject_cast<IODialog *>( parent );

	origSources.clear();
	origSources << sources;

	sourceList.clear();

	mProcess = progress;

	mPaused = false;
	mCanceled = false;

	if ( not mProcess->sourceDir.endsWith( "/" ) )
		mProcess->sourceDir+= "/";

	if ( not mProcess->targetDir.endsWith( "/" ) )
		mProcess->targetDir+= "/";

	/* Initialize the sizes to zero */
	mProcess->totalBytes = 0;
	mProcess->totalBytesCopied = 0;
	mProcess->fileBytes = 0;
	mProcess->fileBytesCopied = 0;

    /* Initialize the state to IOProcess::NotStarted */
    mProcess->state = CPrime::NotStarted;
};

QStringList CIOProcess::errors() {

	return errorNodes;
};

void CIOProcess::cancel() {

    mProcess->state = CPrime::Canceled;
	mCanceled = true;

	emit canceled( errorNodes );
};

void CIOProcess::pause() {

    mProcess->state = CPrime::Paused;
	mPaused = true;
};

void CIOProcess::resume() {

    mProcess->state = CPrime::Started;
	mPaused = false;
};

bool CIOProcess::preIO() {

    mProcess->state = CPrime::Starting;

	if ( not CPrime::FileUtils::isWritable( mProcess->targetDir ) ) {
		emit noWriteAccess();
        mProcess->state = CPrime::Completed;

		return false;
	}

	/* No cut/copy-paste in the same directory */
	if ( mProcess->sourceDir == mProcess->targetDir ) {

        if ( mProcess->type == CPrime::Move )
			CPrime::MessageEngine::messageEngine("dialog-information", "File Transfer", "Info", "Since the source and target directories are the same, the IO will not proceed.");

		else {
			CPrime::MessageEngine::messageEngine("dialog-information", "File Transfer", "Info",
				"Since the source and target directories are the same, the IO will not proceed.<br>"
                "If you intend to make a copy of the folder, please use the option <tt>Duplicate</tt>.");
		}

        mProcess->state = CPrime::Completed;
		return false;
	}

	/* If we are moving file and in the same partition, we can skip the processing */
    if ( mProcess->type == CPrime::Move ) {

		struct stat srcStat, tgtStat;
		stat( mProcess->sourceDir.toLocal8Bit().data(), &srcStat );
		stat( mProcess->targetDir.toLocal8Bit().data(), &tgtStat );

		/* If the source and the are the same */
		if ( srcStat.st_dev == tgtStat.st_dev ) {
			/* Add the original sources to the list */
			sourceList << origSources;

			/* Move is just a trivial rename operation, we can get out of here */
			return true;
		}

		/* Otherwise we proceed with the source processing */
	}

	sourceList.clear();

	/* Obtain the file sizes */
	Q_FOREACH(const QString &src, origSources ) {
		mProcess->progressText = QString( "Processing %1..." ).arg( src );
		if ( CPrime::FileUtils::isDir( mProcess->sourceDir + src ) )
			processDirectory( src );

		else {

			sourceList << src;
			mProcess->totalBytes += CPrime::FileUtils::getFileSize( mProcess->sourceDir + src );
		}
	}

	/* Check if we have enough space to perform the IO */
	mProcess->progressText = QString( "Checking space requirements..." );
	QStorageInfo tgtInfo( mProcess->targetDir );

	if ( tgtInfo.bytesAvailable() <= qint64( mProcess->totalBytes ) ) {

		emit noSpace();

		CPrime::MessageEngine::messageEngine("dialog-information", "File Transfer", "Info", "Not enough space in the target directory.");

        mProcess->state = CPrime::Completed;
		return false;
	}

	return true;
};

void CIOProcess::processDirectory( QString path ) {

	DIR* d_fh;
	struct dirent* entry;

    while ( ( d_fh = opendir( ( mProcess->sourceDir + path ).toLocal8Bit().data() ) ) == nullptr ) {
		qWarning() << "Couldn't open directory:" << path;
		return;
	}

	if ( not path.endsWith( "/" ) )
		path += "/";

	/* Create this path at the target */
	CPrime::FileUtils::mkpath( mProcess->targetDir + path, 0755 );

	/* Now, we can read what is inside this directory */
    while( ( entry = readdir( d_fh ) ) != nullptr ) {

		/* Don't descend up the tree or include the current directory */
		if ( strcmp( entry->d_name, ".." ) != 0 && strcmp( entry->d_name, "." ) != 0 ) {

			if ( entry->d_type == DT_DIR ) {

				/* Stat the directory to get the mode */
				struct stat st;
				stat( ( mProcess->sourceDir + path + entry->d_name ).toLocal8Bit().data(), &st );

				/* Create this directory at the target */
				CPrime::FileUtils::mkpath( mProcess->targetDir + path + entry->d_name, st.st_mode );

				/* Recurse into that folder */
				processDirectory( path + entry->d_name );
			}

			else {

				/* Get the size of the current file */
				mProcess->totalBytes += CPrime::FileUtils::getFileSize( mProcess->sourceDir + path + entry->d_name );

				/* Add this to the source file list */
				sourceList << path + entry->d_name;
			}
		}
	}

	closedir( d_fh );
};

void CIOProcess::copyFile( QString srcFile ) {

	QThread::setPriority( QThread::LowestPriority );

	char buffer[ BUFSIZ ];

	qint64 inBytes = 0;
	qint64 bytesWritten = 0;

	QString currentFile = mProcess->targetDir + srcFile;

	if ( not CPrime::FileUtils::isReadable( mProcess->sourceDir + srcFile ) ) {
		qDebug() << "Unreadable file:" << srcFile;
		errorNodes << srcFile;
		return;
	}

	if ( not CPrime::FileUtils::isWritable( CPrime::FileUtils::dirName( currentFile ) ) ) {
		qDebug() << currentFile << "not writable!!!";
		errorNodes << srcFile;
		return;
	}

	/* If the file exists, ask the user what is to be done */
	/* 0 - Ask every time or not yet asked */
	/* 1 - Replace all */
	/* 2 - Keep all */
	if ( CPrime::FileUtils::exists( currentFile ) ) {
		/* Cut-or-Copy/paste in the same folder: Should not come here */
		if ( mProcess->sourceDir == mProcess->targetDir ) {
			resolution = QMessageBox::No;
		}

		/* Unresolved */
		if ( not mResolveConflict ) {
			/* Wait resolution */
			if ( resolution == QMessageBox::NoButton )
				emit resolveConflict( currentFile, mProcess->sourceDir + srcFile );

			while ( resolution == QMessageBox::NoButton ) {
				usleep( 100 );
				qApp->processEvents();
			}

			/* Keep existing; mResolveConflict = 0 */
			if ( resolution == QMessageBox::No ) {
				mResolveConflict = 0;
				currentFile = newFileName( currentFile );
			}

			/* Keep all existing; mResolveConflict = 2 */
			else if ( resolution == QMessageBox::NoToAll ) {
				mResolveConflict = 2;
				currentFile = newFileName( currentFile );
			}

			/* Ignore: forget copying the current file; mResolveConflict = 0 */
			else if ( resolution == QMessageBox::Ignore ) {
				mResolveConflict = 0;
                if ( mProcess->type == CPrime::Move )
					errorNodes <<  srcFile;

				return;
			}

			/* Replace: remove existing (assume we can delete), then copy; mResolveConflict = 0 */
			else if ( resolution == QMessageBox::Yes ) {
				mResolveConflict = 0;
				QFile::remove( currentFile );
			}

			/* Replace all: remove existing (assume we can delete), then copy; mResolveConflict = 1 */
			else if ( resolution == QMessageBox::YesToAll ) {
				mResolveConflict = 1;
				QFile::remove( currentFile );
			}

			else {
				/* Should never come here; but same QMessageBox::Ignore */
				mResolveConflict = 0;
				return;
			}

			resolution = QMessageBox::NoButton;
		}

		/* Replace all: remove existing file */
		else if ( mResolveConflict == 1 )
			QFile::remove( currentFile );

		/* Keep all: Rename current */
		else if ( mResolveConflict == 2 )
			currentFile = newFileName( currentFile );

		/* Ignore: forget the copying */
		else
			return;
	}

	struct stat iStat, oStat;
	stat( ( mProcess->sourceDir + srcFile ).toLocal8Bit().data(), &iStat );
	stat( mProcess->targetDir.toLocal8Bit().data(), &oStat );

	/* Open the input file descriptor fro reading */
	int iFileFD = open( ( mProcess->sourceDir + srcFile ).toLocal8Bit().data(), O_RDONLY );

	/* Open the output file descriptor for reading */
	int oFileFD = open( currentFile.toLocal8Bit().data(), O_WRONLY | O_CREAT, iStat.st_mode );

    /* IOProcess::Progress::fileBytes */
    mProcess->fileBytes = static_cast<quint64>(iStat.st_size);

    /* IOProcess::Progress::fileBytesCopied */
	mProcess->fileBytesCopied = 0;

	/* While we read positive chunks of data we write it */
	while ( ( inBytes = read( iFileFD, buffer, BUFSIZ ) ) > 0 ) {
		if ( mCanceled ) {
			close( iFileFD );
			close( oFileFD );

			emit canceled( errorNodes );

			return;
		}

		while ( mPaused ) {
			if ( mCanceled ) {
				close( iFileFD );
				close( oFileFD );

				emit canceled( errorNodes );

				return;
			}

			usleep( 100 );
			qApp->processEvents();
		}

        bytesWritten = write( oFileFD, buffer, static_cast<size_t>(inBytes) );

		if ( bytesWritten != inBytes ) {
			qDebug() << "Error writing to file:" << currentFile;
			qDebug() << "[Error]:" << strerror( errno );
			errorNodes << srcFile;
			break;
		}

        mProcess->fileBytesCopied += static_cast<quint64>(bytesWritten);
        mProcess->totalBytesCopied += static_cast<quint64>(bytesWritten);
	}

	close( iFileFD );
	close( oFileFD );

	/* If read(...) resulted in an error */
	if ( inBytes == -1 ) {
		qDebug() << "Error copying file:" << srcFile;
		qDebug() << "[Error]:" << strerror( errno );
		errorNodes << srcFile;
	}

	if ( mProcess->fileBytesCopied != quint64( iStat.st_size ) )
		errorNodes << srcFile;
};

QString CIOProcess::newFileName( QString fileName ) {

	int i = 0;
	QString newFile;

	do {
		newFile = CPrime::FileUtils::dirName( fileName ) + QString( "/Copy (%1) - " ).arg( i ) + CPrime::FileUtils::baseName( fileName );
		i++;
	} while( CPrime::FileUtils::exists( newFile ) );

	return newFile;
};

void CIOProcess::run() {

	if ( mCanceled ) {
		emit canceled( errorNodes );

		quit();
		return;
	}

	while ( mPaused ) {
		if ( mCanceled ) {
			emit canceled( errorNodes );

			return;
		}

		usleep( 100 );
		qApp->processEvents();
	}

	/* First we process the sources */
	if ( not preIO() ) {

		emit completed( QStringList() );

		quit();
		return;
	}

	/* Actual IO Begins */

	mProcess->progressText = QString();
    mProcess->state = CPrime::Started;

    if ( mProcess->type == CPrime::Move ) {
		struct stat srcStat, tgtStat;
		stat( mProcess->sourceDir.toLocal8Bit().data(), &srcStat );
		stat( mProcess->targetDir.toLocal8Bit().data(), &tgtStat );

		/* If the source and the target devices are the same */
		QStringList moveList( sourceList );
		if ( srcStat.st_dev == tgtStat.st_dev ) {
			for( int i = 0; i < moveList.count(); i++ ) {
				QString node = moveList.value( i );

				QString srcNode = mProcess->sourceDir + node;
				QString tgtNode = mProcess->targetDir + node;

				if ( not CPrime::FileUtils::exists( tgtNode ) ) {
					if ( QFile::rename( srcNode, tgtNode ) )
						sourceList.removeAt( i );

					else {
						QString node = sourceList.takeAt( i );
						if ( CPrime::FileUtils::isDir( srcNode ) )
							processDirectory( node );

						else
							mProcess->totalBytes += CPrime::FileUtils::getFileSize( node );
					}
				}

				else {
					QString node = sourceList.takeAt( i );
					if ( CPrime::FileUtils::isDir( srcNode ) )
						processDirectory( node );

					else
						mProcess->totalBytes += CPrime::FileUtils::getFileSize( node );
				}
			}

			/* If all files have been moved, then signal the end */
			if ( not sourceList.count() ) {
				emit completed( errorNodes );
                mProcess->state = CPrime::Completed;

				quit();
				return;
			}

			/* Some files might not have been moved, try copying them. */
		}

		/* Otherwise, we let the copying take place, then delete the sources at the end. */
	}

	/* Copying: Perform the IO */
	Q_FOREACH(const QString &node, sourceList ) {

		/* Update the current file */
		mProcess->currentFile = node;

		if ( mCanceled ) {
			emit canceled( errorNodes );

			quit();
			return;
		}

		while ( mPaused ) {
			if ( mCanceled ){
				emit canceled( errorNodes );

				quit();
				return;
			}

			usleep( 100 );
			qApp->processEvents();
		}

		struct stat st;
		if ( stat( ( mProcess->sourceDir + node ).toLocal8Bit().data(), &st ) != 0 ) {
			qDebug() << "Stat failed" << node;
			qDebug() << "[Error]:" << strerror( errno );
			errorNodes << node;
		}

		/* Various cases for various types of nodes */
		switch( st.st_mode & S_IFMT ) {

			case S_IFREG: {

				/* Copy a regular file */
				copyFile( node );
				break;
			}

			case S_IFLNK: {

				/* Create a symbolic link */
				symlink( CPrime::FileUtils::readLink( node ).toLocal8Bit().data(), ( mProcess->targetDir + node ).toLocal8Bit().data() );
				if ( not CPrime::FileUtils::exists( ( mProcess->targetDir + node ) ) ) {
					qDebug() << "Error creating symlink (symlink(...))" << node << "->" << CPrime::FileUtils::readLink( node );
					qDebug() << "[Error]:" << strerror( errno );
					errorNodes << node;
				}
				break;
			}

			case S_IFBLK:
			case S_IFCHR:
			case S_IFIFO: {

				/* Create a block device, character special, fifo */
				mknod( ( mProcess->targetDir + node ).toLocal8Bit().data(), st.st_mode, st.st_dev );
				break;
			}

			case S_IFSOCK: {

				qDebug() << "Cannot copy a socket:" << node;
				errorNodes << node;
				break;
			}
		}
	}

    if ( mProcess->type == CPrime::Move ) {
		qDebug() << errorNodes;
		Q_FOREACH(const QString &node, sourceList ) {
			qDebug() << node << errorNodes.contains( node );
			/* If the source was not copied properly */
			if ( errorNodes.contains( node ) )
				continue;

			/* sourceList will be just a list of files */
			if ( unlink( ( mProcess->sourceDir + node ).toLocal8Bit().data() ) != 0 ) {
				qDebug() << "Error removing original file:" << mProcess->sourceDir + node;
				qDebug() << "[Error]:" << strerror( errno );
			}
		}

		/* Mixture of files and folders */
		Q_FOREACH(const QString &node, origSources ) {
			/* If the source was not copied properly */
			if ( errorNodes.contains( node ) )
				continue;

			/* Deletion of folders */
			if ( CPrime::FileUtils::isDir( mProcess->sourceDir + node ) ) {
				/* If a file in this directory was not copied, do not delete it */
				if ( errorNodes.filter( node ).count() )
					continue;

				if ( not CPrime::FileUtils::removeDir( mProcess->sourceDir + node ) ) {
					qDebug() << "Error removing original directory:" << mProcess->sourceDir + node;
					qDebug() << "[Error]:" << strerror( errno );
				}
			}

			/* Deletion of files */
			else if ( unlink( ( mProcess->sourceDir + node ).toLocal8Bit().data() ) != 0 ) {
				qDebug() << "Error removing original file:" << mProcess->sourceDir + node;
				qDebug() << "[Error]:" << strerror( errno );
			}
		}
	}

	emit completed( errorNodes );
    mProcess->state = CPrime::Completed;

	quit();
};

QMessageBox::StandardButton ConflictDialog::resolveConflict(const QString &fileName, QWidget *parent ) {

	QString title = QString( "CoreFM - File Exists" );
	QString message = QString(
		"<p>The file you are trying to copy</p><center><b>%1</b></center>"
		"<p>already exists in the target directory. What would you like to do?</p>"
		"<tt>[Yes]</tt> - Replace<br>"
		"<tt>[Yes to All]</tt> - Replace all existing files<br>"
		"<tt>[No]</tt> - Keep both files<br>"
		"<tt>[No to All]</tt> - Keep all existing files<br>"
		"<tt>[Ignore]</tt> - Skip copying files if they exist"
	).arg( CPrime::FileUtils::baseName( fileName ) );

	QMessageBox::StandardButtons buttons = QMessageBox::Yes | QMessageBox::YesToAll | QMessageBox::No | QMessageBox::NoToAll | QMessageBox::Ignore;

	return QMessageBox::question( parent, title, message, buttons, QMessageBox::No );
};

IODialog::IODialog( QStringList sources, CPrime::IOProcess *process ) : QDialog() {

	mProcess = process;

	QLabel *title = new QLabel( this );
    if ( process->type == CPrime::Copy )
		title->setText( "<h4>Copying</h4>" );

	else
		title->setText( "<h4>Moving</h4>" );

	QLabel *source = new QLabel( "Source: <b>" + process->sourceDir + "</b>" );
	QLabel *target = new QLabel( "Target: <b>" + process->targetDir + "</b>" );

	pBar = new QProgressBar( this );
	pBar->setFormat( "Processing..." );

	/* Fusion style shows text inside the progressbar. */
	pBar->setStyle( QStyleFactory::create( "fusion" ) );

	ioproc = new CIOProcess( sources, process, this );
	connect( ioproc, SIGNAL( resolveConflict( QString, QString ) ), this, SLOT( resolveConflict( QString, QString ) ) );
	connect( ioproc, SIGNAL( completed( QStringList ) ), this, SLOT( close() ) );

	pauseBtn = new QPushButton( QIcon::fromTheme( "media-playback-pause" ), "Pause", this );
	connect( pauseBtn, SIGNAL( clicked() ), this, SLOT( togglePause() ) );

	QPushButton *cancelBtn = new QPushButton( QIcon::fromTheme( "dialog-close" ), "Cancel", this );
	connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( cancelIO() ) );

	QVBoxLayout *lblLyt = new QVBoxLayout();
	lblLyt->addWidget( title );
	lblLyt->addWidget( source );
	lblLyt->addWidget( target );

	QHBoxLayout *btnLyt = new QHBoxLayout();
	btnLyt->addStretch();
	btnLyt->addWidget( pauseBtn );
	btnLyt->addWidget( cancelBtn );

	QVBoxLayout *dlgLyt = new QVBoxLayout();
	dlgLyt->addLayout( lblLyt );
	dlgLyt->addWidget( pBar );
	dlgLyt->addLayout( btnLyt );

	setLayout( dlgLyt );

	process->startTime = QDateTime::currentDateTime();
	ioproc->start();

	/* Update every 250 ms */
	timer.start( 250, this );
};

void IODialog::copy(QString &source, const QString &target ) {

    CPrime::IOProcess *process = new CPrime::IOProcess;
	process->sourceDir = CPrime::FileUtils::dirName( source );
	process->targetDir = target;

	if ( not process->sourceDir.endsWith( "/" ) )
		process->sourceDir += "/";

    process->type = CPrime::Copy;

	IODialog *pasteDlg = new IODialog( QStringList() << source.replace( process->sourceDir, "" ), process );
	pasteDlg->show();
};

void IODialog::show() {

    if ( mProcess->state == CPrime::Completed )
		return;

	QDialog::show();
};

void IODialog::togglePause() {

	/* Already paused, so we resume it, set pause text and icon */
    if ( mProcess->state == CPrime::Paused ) {
		ioproc->resume();

		pauseBtn->setIcon( QIcon::fromTheme( "media-playback-pause" ) );
		pauseBtn->setText( "Pause" );
	}

	else {
		ioproc->pause();

		pauseBtn->setIcon( QIcon::fromTheme( "media-playback-start" ) );
		pauseBtn->setText( "Resume" );
	}
};

void IODialog::cancelIO() {

	int reply = QMessageBox::question(
		this,
		"CoreFM - Abort?",
		"<p>Are you sure you want to cancel the current IO process?</p>"
		"Press <tt>[Yes]</tt>, to cancel the IO, <tt>[No]</tt> to continue the copying.",
		QMessageBox::Yes | QMessageBox::No,
		QMessageBox::No
	);

	if ( reply == QMessageBox::Yes ) {
		ioproc->cancel();
		close();
	}
};

void IODialog::resolveConflict( QString file, QString ) {

	ioproc->resolution = ConflictDialog::resolveConflict( file, this );
};

void IODialog::timerEvent( QTimerEvent *tEvent ) {

    if ( mProcess->state == CPrime::Completed )
		return tEvent->ignore();

	if ( tEvent->timerId() == timer.timerId() ) {
		if ( not mProcess->totalBytes )
			mProcess->totalBytes = 1;

		switch( mProcess->state ) {
            case CPrime::NotStarted: {
				pBar->setFormat( "Waiting..." );
				pBar->setRange( 0, 1 );

				break;
			}

            case CPrime::Starting: {
				pBar->setFormat( "Processing ..." );
				pBar->setRange( 0, 0 );

				break;
			}

            case CPrime::Started: {
				pBar->setFormat( "%p% completed." );
				pBar->setRange( 0, 100 );
                pBar->setValue( static_cast<int>(mProcess->totalBytesCopied * 100 / mProcess->totalBytes) );

				break;
			}

            case CPrime::Paused: {
				pBar->setFormat( "%p% completed (paused)." );
				pBar->setRange( 0, 100 );
                pBar->setValue( static_cast<int>(mProcess->totalBytesCopied * 100 / mProcess->totalBytes ));

				break;
			}

            case CPrime::Canceled: {
				pBar->setFormat( "%p% completed (canceled)." );
				pBar->setRange( 0, 100 );
                pBar->setValue( static_cast<int>(mProcess->totalBytesCopied * 100 / mProcess->totalBytes) );

				timer.stop();

				break;
			}

            case CPrime::Completed: {
				pBar->setFormat( "%p% completed." );
				pBar->setRange( 0, 100 );
                pBar->setValue( static_cast<int>(mProcess->totalBytesCopied * 100 / mProcess->totalBytes ));

				timer.stop();

				break;
			}
		}

		return;
	}

	QDialog::timerEvent( tEvent );
};

void IODialog::closeEvent( QCloseEvent *cEvent ) {

	if ( ioproc->isRunning() ) {
		cancelIO();
        if ( mProcess->state != CPrime::Canceled ) {
			cEvent->ignore();
			return;
		}
	}

    if ( mProcess->state == CPrime::Canceled ){
		CPrime::MessageEngine::messageEngine("dialog-warning", "File Transfer", "Warning!!!", "The IO process was cancelled.");
    }

    else if ( mProcess->state == CPrime::Completed ){
		CPrime::MessageEngine::messageEngine("dialog-information", "File Transfer", "Info", "The IO process completed successfully.");
    }

	cEvent->accept();
};
