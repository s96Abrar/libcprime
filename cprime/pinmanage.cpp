/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QSettings>
#include <QDateTime>

#include "filefunc.h"
#include "variables.h"
#include "pinmanage.h"


PinManage::PinManage()
{
	CPrime::FileUtils::setupFolder(CPrime::FolderSetup::ConfigFolder);
	pinSettings = new QSettings(CPrime::Variables::CC_PinsFilePath(), QSettings::NativeFormat);
}

PinManage::~PinManage()
{
	delete pinSettings;
}

QStringList PinManage::getPinSections()
{
	QStringList sections = pinSettings->childGroups();
	if (sections.isEmpty())
		addSection("Speed Dial");

	return pinSettings->childKeys();
}

QStringList PinManage::getPinNames(const QString &sectionName)
{
	pinSettings->beginGroup(sectionName);
	QStringList list = pinSettings->childKeys();
	pinSettings->endGroup();
	return list;
}

void PinManage::addSection(const QString &sectionName)
{
	// Create a section on General group
	pinSettings->setValue(sectionName, 0); // '0' doesn't mean anything
	pinSettings->sync();
}

void PinManage::addPin(const QString &sectionName, const QString &pinName, const QString &pinpath)
{
	QString value = pinpath + "\t\t\t" + QDateTime::currentDateTime().toString("hh.mm.ss - dd.MM.yyyy");
	pinSettings->setValue(QString("%1/%2").arg(sectionName).arg(pinName), value);
	pinSettings->sync();
}

void PinManage::delSection(const QString &sectionName)
{
	pinSettings->remove(sectionName);
	pinSettings->sync();
}

void PinManage::delPin(const QString &pinName, const QString &section)
{
	pinSettings->remove(QString("%1/%2").arg(section).arg(pinName));
	pinSettings->sync();
}

void PinManage::editPin(const QString &sectionName, const QString &pinName, const QString &pinpath)
{
    addPin(sectionName, pinName, pinpath);
}

void PinManage::changeAll(const QString &oldSectionName, const QString &oldpinName,
						  const QString &sectionName, const QString &pinName, const QString &pinValue)
{
    delPin(oldpinName, oldSectionName);
    addPin(sectionName, pinName, pinValue);
}

QString PinManage::pinPath(const QString &sectionName, const QString &pinName)
{
    QStringList values(pinValues(sectionName, pinName).split("\t\t\t"));
    return values.at(0);
}

QString PinManage::piningTime(const QString &sectionName, const QString &pinName)
{
    QStringList values(pinValues(sectionName, pinName).split("\t\t\t"));
    return values.at(1);
}

QString PinManage::checkingPinName(const QString &sectionName, const QString &pinName)
{
    if (getPinNames(sectionName).contains(pinName, Qt::CaseInsensitive)) {
        return "Pin exists.";
    } else {
        return "";
    }
}

// Check on specific section
QString PinManage::checkingPinPath(const QString &sectionn, const QString &pinpath)
{
	Q_FOREACH (const QString &bName, getPinNames(sectionn)) {
		if (!pinpath.compare(pinPath(sectionn, bName))) {
			return QString("'%1' \nexists in %2 section.").arg(bName).arg(sectionn);
		}
	}

	return "";
}

// Check all section
QString PinManage::checkingPinPathEx(const QString &pinpath)
{
	QString message;
	Q_FOREACH (const QString &section, getPinSections()) {
		message = checkingPinPath(section, pinpath);
		if (message.count())
			break;
	}
	return message;
}

QString PinManage::pinValues(const QString &sectionName, const QString &pinName) const
{
	return pinSettings->value(QString("%1/%2").arg(sectionName).arg(pinName)).toString();
}
