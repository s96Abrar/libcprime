/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QWidget>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QCompleter>
#include <QStringListModel>
#include <QDirIterator>
#include <QDebug>
#include <QIcon>
#include <QScroller>

#include "cprime.h"
#include "systemxdg.h"
#include "themefunc.h"
#include "applicationdialog.h"

using namespace CPrime;

ApplicationDialog::ApplicationDialog(const QSize &iconSize, QWidget *parent) : QDialog( parent )
  , m_iconSize(iconSize)
{
    // Title and size
    this->setWindowIcon(QIcon::fromTheme("applications-other"));
    this->setWindowTitle(tr("Select Application"));

    // Creates app list view
    appList = new QTreeWidget(this);
    appList->setIconSize(m_iconSize);
    appList->setFocusPolicy(Qt::NoFocus);
    appList->headerItem()->setText(0, tr("Application"));

    // Creates buttons
    QDialogButtonBox *buttons = new QDialogButtonBox(this);
    buttons->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));

    // Command bar
    edtCommand = new QLineEdit(this);
    edtCommand->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    QFormLayout *layoutCommand = new QFormLayout();
    layoutCommand->addRow(tr("Search: "), edtCommand);
    edtCommand->setFocus();

    // Layout
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(appList);
    layout->addLayout(layoutCommand);
    layout->addWidget(buttons);

    // Synonyms for cathegory names
    catNames.clear();
    catNames.insert("CSuite", QStringList()) ;
    catNames.insert("Development", QStringList() << "Programming");
    catNames.insert("Games", QStringList() << "Game");
    catNames.insert("Graphics", QStringList());
    catNames.insert("Internet", QStringList() << "Network" << "WebBrowser");
    catNames.insert("Multimedia", QStringList() << "AudioVideo" << "Video");
    catNames.insert("Office", QStringList());
    catNames.insert("Other", QStringList());
    catNames.insert("Settings", QStringList() << "System");
    catNames.insert("Utilities", QStringList() << "Utility");


    // Create default application cathegories
    categories.clear();
    createCategories();

    // Load applications and create category tree list
    AppsList apps = SystemXdgMime::instance()->allDesktops();

	Q_FOREACH(const DesktopFile &app, apps ) {

        // Do we have a valid app?
        if ( not app.isValid() )
            continue;

        // Ignore apps with NoDisplay key set to true
        if ( not app.visible() )
			continue;

        // Find category
        QTreeWidgetItem *category = findCategory(app);

        // Create item from current mime
        QTreeWidgetItem *item = new QTreeWidgetItem(category);
        item->setData(0, Qt::UserRole, app.desktopName());
        item->setIcon(0, CPrime::ThemeFunc::getAppIcon(app.desktopName()));
        item->setText(0, app.name());
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

        // Register application
        QString desktopName = app.name();
        applications.insert(item, desktopName);
    }

    appList->setSortingEnabled(true);
    appList->sortByColumn(0, Qt::AscendingOrder);

    // Create completer and its model for editation of command
    QStringListModel *model = new QStringListModel(this);
    model->setStringList(applications.values());
    QCompleter *completer = new QCompleter(this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setModel(model);
    edtCommand->setCompleter(completer);

    QScroller::grabGesture(appList, QScroller::TouchGesture);

	// Clean the empty category
	int index = 0;
	while (index < appList->topLevelItemCount()) {
		if (!appList->topLevelItem(index)->childCount()) {
			categories.remove(appList->topLevelItem(index)->text(0));
			delete appList->takeTopLevelItem(index);
			continue;
		}
		index++;
	}

    // Signals
    connect(appList,
            SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)),
            SLOT(updateCommand(QTreeWidgetItem *, QTreeWidgetItem *)));
}

ApplicationDialog::~ApplicationDialog()
{
    delete appList;
    delete edtCommand;
    catNames.clear();
    categories.clear();
    applications.clear();
}

/**
 * @brief Returns currently selected launcher
 * @return currently selected launcher
 */
QString ApplicationDialog::getCurrentLauncher() const
{
    QString desktopName = applications.key(edtCommand->text())->data(0, Qt::UserRole).toString();
    return desktopName.left(desktopName.size() - 8);
//    return edtCommand->text();
}

/**
 * @brief Creates default application categories
 * @param names names of cathegories with synonyms
 */
void ApplicationDialog::createCategories()
{
    // Create cathegories
	Q_FOREACH (const QString &name, catNames.keys()) {
        qDebug() << "Cat. Name " << name;

        // Find icon
        QIcon icon = QIcon::fromTheme("applications-" + name.toLower());

        if (icon.isNull()) {
            qDebug() << "Icon null";
        }

        // If icon not found, check synonyms
        if (icon.isNull()) {
			Q_FOREACH (const QString &synonym, catNames.value(name)) {
                icon = QIcon::fromTheme("applications-" + synonym.toLower());
                break;
            }
        }

        // If icon still not found, retrieve default icon
        if (icon.isNull()) {
            icon = QIcon::fromTheme("application-x-executable");
        }

        // Create category
        QTreeWidgetItem *category = new QTreeWidgetItem(appList);
        category->setText(0, name);
        category->setIcon(0, icon);
        category->setFlags(Qt::ItemIsEnabled);
        categories.insert(name, category);
    }
}

/**
 * @brief Searches the most suitable category for application
 * @param app
 * @return cathegory
 */
QTreeWidgetItem *ApplicationDialog::findCategory(const DesktopFile &app)
{
   // Default categoty is 'Other'
    QTreeWidgetItem *category = categories.value("Other");

    // Try to find more suitable category
	Q_FOREACH (const QString &name, catNames.keys()) {

        // Try category name
        if (app.categories().contains(name)) {
            category = categories.value(name);
            break;
        }

        // Try synonyms
        bool found = false;

		Q_FOREACH (const QString &synonym, catNames.value(name)) {
            if (app.categories().contains(synonym)) {
                found = true;
                break;
            }
        }

        if (found) {
            category = categories.value(name);
            break;
        }
    }

    return category;
}

/**
 * @brief Updates launcher command
 * @param currentv
 * @param previous
 */
void ApplicationDialog::updateCommand(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    Q_UNUSED(previous)
    edtCommand->setText(applications.value(current));
}
