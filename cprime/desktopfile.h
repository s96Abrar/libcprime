/*
	*
	* This file is a part of Libcprime.
	* Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, vsit http://www.gnu.org/licenses/.
	*
*/

#pragma once

#include "libcprime_global.h"

namespace CPrime {

class LIBCPRIMESHARED_EXPORT DesktopFile {

public:
	enum Type {
		Application = 0x906AF2,							// A regular executable app
		Link,											// Linux equivalent of '.lnk'			! NOT HANDLED
		Directory										// Desktopthat points to a directory	! NOT HANDLED
	};

	explicit DesktopFile(const QString &filename = QString());	// Create an instance of a desktop file

	bool startApplication();
	bool startApplicationWithArgs(QStringList);

	QString desktopName()const;							// Filename of the desktop
	QString name() const;								// Name
	QString genericName() const;						// Generic Name
	QString description() const;						// Comment
	QString exec() const;								// 'TryExec' value or the path divined from 'Exec'
	QString command() const;							// Full command as given in 'Exec'
	QString icon() const;								// Application Icon Name or Path

	QStringList mimeTypes() const;						// MimeTypes handled by this app
	QStringList categories() const;						// Categories this app belongs to
	QStringList parsedExec() const;						// Arguments parsed with %U, %F, %u, %f etc removed

	int type() const;									// Application/Link/Directory
	int rank() const;

	bool visible() const;								// Visible in 'Start' Menu
	bool runInTerminal() const;							// If this app should be run in the terminal
	bool multipleArgs() const;							// Does the app take multiple arguments?
	bool isValid() const;								// Is a valid desktop file

	QString desktopFileUrl() const;						//

private:
	QString mFileUrl, mDesktopName, mExec, mCommand;
	QString mName, mGenericName, mDescription, mIcon;
	QStringList mMimeTypes, mCategories, mParsedArgs;

	bool mVisible, mRunInTerminal, mValid = false;
	bool mMultiArgs = false, mTakesArgs = false;

	int mType;

	short int mRank = 3;
};

uint qHash( const DesktopFile &app );

}

Q_DECLARE_METATYPE( CPrime::DesktopFile )
