/*
    *
    * This file is a part of Libcprime
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QIcon>

#include "variables.h"


class LIBCPRIMESHARED_EXPORT WidgetsInterface : public QObject {

	Q_OBJECT

public:
    virtual ~WidgetsInterface() {}

	/* Name of the plugin */
	virtual QString name() = 0;

	/* The plugin version */
	virtual QString version() = 0;

	/* The widget */
	virtual QWidget *widget(QWidget *parent) = 0;

};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(WidgetsInterface, WIDGETS_PLUGININTERFACE)
QT_END_NAMESPACE


class LIBCPRIMESHARED_EXPORT ShareItInterface : public QObject {

	Q_OBJECT

public:
	enum Context {
		File         = 0x929C29,
		Files,
		Dir,
		Dirs,
		All
	};
	Q_ENUM(Context)

	/* Name of the plugin */
	virtual QString name() = 0;

	/* Icon of the plugin */
	virtual QIcon icon() = 0;

	/* MimeTypes handled by the plugin */
	virtual QStringList mimeTypes() = 0;

	/* Context of the plugin */
	virtual Context context() = 0;

	/* Dialog to be shown */
	virtual bool shareItDialog(QStringList files, QWidget *parent) = 0;
};

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(ShareItInterface, SHAREIT_PLUGININTERFACE)
QT_END_NAMESPACE

