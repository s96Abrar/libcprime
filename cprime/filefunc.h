/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QMimeDatabase>
#include <QMimeType>

#include <sys/stat.h>

#include "cenums.h"
#include "libcprime_global.h"

namespace CPrime {

class LIBCPRIMESHARED_EXPORT FileUtils {

public:
	// File sizes in bytes.
	static const quint64 KB = 1024;
	static const quint64 MB = 1024 * KB;
	static const quint64 GB = 1024 * MB;
	static const quint64 TB = 1024 * GB;

	/* Precision: Number of decimal digits to be shown */
	static QString formatSize( const quint64 &size);
	static double formatSizeRaw(const quint64 &size);
	static QString formatSizeStr( const quint64 &fSize );
	static quint64 maxYForSize( quint64 fileSize );

    static quint64 getFileSize( const QString &path );

	static QString dirName(QString path );
	static QString baseName(QString path );
	static int mkpath(QString path, mode_t mode );

	static bool isFile(const QString &path );
	static bool isDir(const QString &path );
	static bool isLink(const QString &path );
	static bool exists(const QString &path );

	static bool isReadable(const QString &path );
	static bool isWritable(const QString &path );

	static bool removeDir(const QString &dirName );

	static QString readLink(const QString &path );

	static QMimeType mimeType(const QString &path );

    static QString checkIsValidDir(const QString &path);
    static QString checkIsValidFile(const QString &file);
    static bool setupFolder(CPrime::FolderSetup fs);
};

}
