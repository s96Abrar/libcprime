/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QSettings>
#include <QFile>

#include "activitesmanage.h"
#include "sortfunc.h"
#include "variables.h"
#include "filefunc.h"


bool CPrime::ActivitiesManage::saveToActivites(const QString &appName, const QStringList &paths)
{
	CPrime::FileUtils::setupFolder(CPrime::ConfigFolder);

	if (!appName.count() || !paths.count()) {
		return false;
	}

	QSettings recentActivity(CPrime::Variables::CC_ActivitiesFilePath(), QSettings::IniFormat);

	QMap<QString, QString> activityMap;

	QStringList allKeys = recentActivity.allKeys();
	Q_FOREACH (const QString &key, allKeys) {
		activityMap[recentActivity.value(key).toString()] = key;
		recentActivity.remove(key); // Deleting all the keys
	}

	// Saving the map (very bad approach)
	Q_FOREACH (const QString &value, activityMap.keys()) {
		recentActivity.setValue(activityMap[value], value);
	}

	QDateTime currentDT = QDateTime::currentDateTime();
	Q_FOREACH (const QString &path, paths) {
		currentDT = currentDT.addMSecs(1); // Add a litte delay to make them separate
		QString group = currentDT.toString("dd.MM.yyyy");
		QString key = currentDT.toString("hh.mm.ss.zzz");
		QString value = appName + "\t\t\t" + path;

		if (activityMap.contains(value)) {
			recentActivity.remove(activityMap[value]);
		}

		recentActivity.sync();
		recentActivity.beginGroup(group);
		recentActivity.setValue(key, value);
		recentActivity.endGroup();

	}

	int count = recentActivity.childGroups().count();
	int limitedCount = 200; // Still concerning 200 items is so much
	if (count > limitedCount) {
		QStringList list = SortFunc::sortDateTime(recentActivity.allKeys(), CPrime::Descending, "dd.MM.yyyy/hh.mm.ss.zzz");

		for (int i = count - 1; i >= limitedCount; i--) {
			recentActivity.remove(list[i]);
		}
	}

	return true;
}

//bool CPrime::ActivitiesManage::deleteActivites(const QString &appName)
//{
//	// TODO: Delete only one apps activites
//	// Under construction
//	(void) appName;
//	return false;
//}

bool CPrime::ActivitiesManage::deleteAllActivites()
{
	// Delete all apps activites
	QFile actFile(CPrime::Variables::CC_ActivitiesFilePath());

	if (actFile.exists()) {
		return actFile.remove();
	}

	return false;
}
