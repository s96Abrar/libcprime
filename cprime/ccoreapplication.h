/*
	*
	* This file is a part of PaperSessionManager.
	* PaperSessionManager is the Session Manager for PaperDesktop
	* Copyright 2020 CuboCore Group
	*

	*
	* This file is derived from QSingleApplication, originally written
	* as a part of Qt Solutions. For license read DesQApplication.cpp
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QtCore>

class QtLocalPeer;

namespace CPrime {

class CCoreApplication : public QCoreApplication {
    Q_OBJECT

	public:
		CCoreApplication( int &argc, char **argv );
		CCoreApplication( const QString &id, int &argc, char **argv );

		bool isRunning();
		QString id() const;

	public Q_SLOTS:
		bool sendMessage( const QString &message, int timeout = 5000 );
		void disconnect();

	Q_SIGNALS:
		void messageReceived( const QString &message );

	private:
		void sysInit( const QString &appId = QString() );
		QtLocalPeer *peer;
};

}
