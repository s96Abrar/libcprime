/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QDialog>
#include <QMap>

#include "libcprime_global.h"

class QWidget;
class QTreeWidget;
class QTreeWidgetItem;
class QLineEdit;
class DesktopFile;

namespace CPrime {

class LIBCPRIMESHARED_EXPORT ApplicationDialog : public QDialog {

    Q_OBJECT

public:

    /**
     * @brief Call Application Dialog dialog from any apps by giving the path to it.
     * @param iconSize : Set icon size of the list in the dialog.
     * @param returns : Returs app exc name.
     */
    explicit ApplicationDialog(const QSize &iconSize = QSize(), QWidget *parent = nullptr);

    ~ApplicationDialog();
    QString getCurrentLauncher() const;

protected slots:
    void updateCommand(QTreeWidgetItem *current, QTreeWidgetItem *previous);

private:
    QTreeWidget *appList;
    QSize m_iconSize;
    QLineEdit *edtCommand;
    QMap<QString, QStringList> catNames;
    QMap<QString, QTreeWidgetItem *> categories;
    QMap<QTreeWidgetItem *, QString> applications;
    QTreeWidgetItem *findCategory(const DesktopFile &app);
    void createCategories();

};

}
