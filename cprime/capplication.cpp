/*
	*
	* This file is a part of PaperSessionManager.
	* PaperSessionManager is the Session Manager for PaperDesktop
	* Copyright 2020 CuboCore Group
	*

	*
	* This file is derived from QSingleApplication, originally written
	* as a part of Qt Solutions. For license read DesQApplication.cpp
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QWidget>

#include "capplication.h"
#include "qtlocalpeer.h"

namespace CPrime {

void CApplication::sysInit( const QString &appId ) {

    actWin = 0;
    peer = new QtLocalPeer( this, appId );
    connect( peer, SIGNAL( messageReceived( const QString& ) ), SIGNAL( messageReceived( const QString& ) ) );
}

CApplication::CApplication( int &argc, char **argv, bool GUIenabled ) : QApplication( argc, argv, GUIenabled ) {

    sysInit();
}

CApplication::CApplication( const QString &appId, int &argc, char **argv ) : QApplication( argc, argv ) {

    sysInit( appId );
}

bool CApplication::isRunning() {

    return peer->isClient();
}

bool CApplication::sendMessage( const QString &message, int timeout ) {

    return peer->sendMessage( message, timeout );
}

QString CApplication::id() const {

    return peer->applicationId();
}

void CApplication::setActivationWindow( QWidget* aw, bool activateOnMessage ) {

    actWin = aw;
    if ( activateOnMessage )
        QObject::connect( peer, SIGNAL( messageReceived( const QString& ) ), this, SLOT( activateWindow() ) );

    else
        QObject::disconnect( peer, SIGNAL( messageReceived( const QString& ) ), this, SLOT( activateWindow() ) );
}

QWidget* CApplication::activationWindow() const {

    return actWin;
}

void CApplication::activateWindow() {

    if ( actWin ) {
        actWin->setWindowState( actWin->windowState() & ~Qt::WindowMinimized );
        actWin->raise();
        actWin->activateWindow();
    }
}

void CApplication::disconnect() {

	peer->shutdown();
}

}
