/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
    * Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QSettings>
#include <QProcess>
#include <QFileInfo>
#include <QWidget>
#include <QUrl>

#include "variables.h"
#include "appopenfunc.h"
#include "applicationdialog.h"
#include "systemxdg.h"
#include "filefunc.h"
#include "messageengine.h"

using namespace CPrime;

/* SystemXdgMime instance */
static SystemXdgMime *cMimeHandler = SystemXdgMime::instance();

/*
 * Open app using QProcess with arguments.
 * appName : Exact name
 */
void CPrime::AppOpenFunc::systemAppOpener(const QString &appName, const QStringList &args)
{
	DesktopFile df(appName);

	if (not df.startApplicationWithArgs(args)) {
		qDebug() << "func(systemAppOpener) : Something has gone wrong; App could not be opened with args:"
				 << appName << args;
		return;
	}

	qDebug() << "func(systemAppOpener) : Opening" << appName << "with args" << args;
}

/*
 * Catagorize a path with the extention to open on specific app
 * path : Need a specific path to app
 */
void CPrime::AppOpenFunc::appOpenEngine(const QString &path)
{
	if (!path.count() || !CPrime::FileUtils::exists(path)) {
		qDebug() <<  "Warning!!! File does not exist...";
		return;
	}

	/* Check if there exists an app to handle the file @path */
    DesktopFile app = cMimeHandler->xdgDefaultApp(mimeDbInstance.mimeTypeForFile(path));

	bool ret;
	if (not app.isValid()) {
		ret = QProcess::startDetached("xdg-open", QStringList() << path);
	} else {
		ret = app.startApplicationWithArgs({path}); // app.startApplicationWithArgs(QStringList() << path);
	}

	if (ret)
		qDebug() << "func(appOpenEngine) : Opening" << path;
	else
		qDebug() << "Error!!! func(appOpenEngine) : Can't open app from this path: " << path;
}

/*
 * Open app (open the *.desktop file) based on category.
 */
void CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory ctg,
        const QFileInfo &file, const QString &keyword)
{
	QString path(file.absoluteFilePath());

	// selected default app name from settings.
	QString defaultApp = SystemDefaultApps::getDefaultApp(ctg);

	if (!defaultApp.count() || defaultApp == "Nothing") {
		qDebug() << "No default app selected!!!\nSelect a default app to avoid this message.";

        QSize iconSize = QSize(32,32);
		ApplicationDialog *dialog = new ApplicationDialog(iconSize, nullptr);
		dialog->resize(500, 600);
        QString appName;

		if (dialog->exec()) {
			if (dialog->getCurrentLauncher().compare("") != 0) {
				appName = dialog->getCurrentLauncher();
				SystemDefaultApps::setDefaultApp(ctg, appName + ".desktop");
				defaultApp = appName;
				CPrime::MessageEngine::messageEngine("dialog-information", "System", "Info",
													 "You can change Default app by modifing the defaultapp.list file in .config folder");

			}
		} else {
			qDebug() << "No default app selected!!!";
			return;
		}
	}

	// Fetch those have arguments.
	// Start them at first.
	if (ctg == DefaultAppCategory::SearchApp) {
		DesktopFile df = DesktopFile(defaultApp);

		// sometimes the exce is binary path, so we clear out the path
		QString exec = CPrime::FileUtils::baseName(df.exec());

		QProcess proc;

		if (exec == "corehunt") {
			if (keyword.isEmpty()) {
				df.startApplicationWithArgs(QStringList() << "--path" << path);
			} else {
				df.startApplicationWithArgs(QStringList() << "--path" << path << "--pattern" << keyword);
			}
		} else if (exec == "catfish") {
			if (keyword.isEmpty()) {
				df.startApplicationWithArgs(QStringList() << path);
			} else {
				df.startApplicationWithArgs(QStringList() << path << "--fulltext" << keyword);
			}
		} else {
			proc.startDetached(defaultApp, QStringList() << path << keyword);
		}

		qDebug() << defaultApp + " executing.";

		return;
	}

	else if (ctg == DefaultAppCategory::Terminal) {
		DesktopFile df = DesktopFile(defaultApp);

		// sometimes the exce is binary path, so we clear out the path
		QString exec = CPrime::FileUtils::baseName(df.exec());

		// Terminals which take --working-directory switch
		QStringList longwd = { "coreterminal", "gnome-terminal", "terminator", "mate-terminal", "xfce4-terminal" };

		// Terminals which take --workdir switch
		QStringList shortwd = { "qterminal", "konsole", "nbterminal" };

		// Terminals which do not accept work-dir switch
		QStringList badwd = { "xterm" };

		// Special baby of the group: LXTerminal which wants --working-directory=WD
		QProcess proc;

		if (exec == "lxterminal") {
			df.startApplicationWithArgs(QStringList() << "--working-directory=" + path);
		} else if (longwd.contains(exec)) {
			df.startApplicationWithArgs(QStringList() << "--working-directory" << path);
		} else if (shortwd.contains(exec)) {
			df.startApplicationWithArgs(QStringList() << "--workdir" << path);
		} else {
			proc.startDetached("xterm", QStringList() << "-e"  << QString("cd %1 & %2").arg(path).arg("/bin/bash"));
		}

		qDebug() << defaultApp + " executing.";
		return;
	}

	DesktopFile df = DesktopFile(defaultApp);
	df.startApplicationWithArgs(QStringList() << file.absoluteFilePath());

	qDebug() << defaultApp + " executing.";
}

