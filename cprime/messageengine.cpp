/*
    *
    * This file is a part of Libcprime
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QtDBus/QDBusPendingReply>
#include <QLabel>
#include <QVBoxLayout>
#include <QTimer>
#include <QGuiApplication>
#include <QScreen>

#include "messageengine.h"

// Notifier Interfaces
#define INTERFACE "org.freedesktop.Notifications"
#define SERVICE "org.freedesktop.Notifications"
#define PATH "/org/freedesktop/Notifications"

using namespace CPrime;

SystemNotifierInterface::SystemNotifierInterface()
	: QDBusAbstractInterface(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus(), nullptr)
{
}

SystemNotifier *SystemNotifier::mInstance = nullptr;

SystemNotifier *SystemNotifier::instance()
{
	if (not mInstance) {
		mInstance = new SystemNotifier();
	}

	return mInstance;
}

SystemNotifier::SystemNotifier() : QObject()
{
	iface = new SystemNotifierInterface();
	init = true;
}

void SystemNotifier::showNotification(const QString &appName, const QString &iconName,
									  const QString &title, const QString &details, int timeout)
{
	/* The third argument (= 0) is the id that needs to be replaced, 0 means, no replacements. */

	QDBusPendingCall reply = iface->asyncCall("Notify", appName, (quint32)0, iconName, title, details, QStringList(), QVariantMap(), timeout);
	QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);

	connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher *)), this, SLOT(handleErrors(QDBusPendingCallWatcher *)));
}

void SystemNotifier::handleErrors(QDBusPendingCallWatcher *watcher)
{
	QDBusPendingReply<quint32> reply = *watcher;

	if (reply.isError()) {
		qDebug() << reply.error().message();
		emit notificationNotShown();
	}

	watcher->deleteLater();
}

/*
 * MessageBox function.
 */
void CPrime::MessageEngine::messageEngine(const QString &messageIcon,
		const QString &appName, const QString &summary, const QString &body)
{
    /*
        *
        * SystemNotifier class gives out a signal if it fails to show the notification.
        * It can be used to show our custom notification:
        *
        * SystemNotifier *notifier = SystemNotifier::instance();
        * connect( notifier, &SystemNotifier::notificationNotShown, ()[=] {
        *		// Code to show our custom notification
        * } );
        *
        * notifier->showNotification( appName, appIcon, summary, body, 2000 );
        *
    */
    SystemNotifier::instance()->showNotification(appName, messageIcon, summary, body, 2000);
}
