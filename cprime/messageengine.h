/*
    *
    * This file is a part of Libcprime
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QtDBus/QDBusAbstractInterface>
#include <QtDBus/QDBusPendingCall>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusInterface>

#include "cenums.h"
#include "libcprime_global.h"

namespace CPrime {

	class LIBCPRIMESHARED_EXPORT SystemNotifierInterface : public QDBusAbstractInterface {
		Q_OBJECT
	public:
		SystemNotifierInterface();
	};

	class LIBCPRIMESHARED_EXPORT SystemNotifier : public QObject {
		Q_OBJECT
	public:
		static SystemNotifier *instance();

		/* Show a notification */
		void showNotification(const QString &appName, const QString &iconName,
							  const QString &title, const QString &details, int timeout = 2000);

	private:
		SystemNotifier();

		static SystemNotifier *mInstance;
		SystemNotifierInterface *iface;
		bool init;

	private Q_SLOTS:
		void handleErrors(QDBusPendingCallWatcher *);

	Q_SIGNALS:
		void notificationNotShown();
	};

	class LIBCPRIMESHARED_EXPORT MessageEngine {
	public:
        static void messageEngine(const QString &messageIcon, const QString &appName, const QString &summary,
                                  const QString &body);
	};

}
