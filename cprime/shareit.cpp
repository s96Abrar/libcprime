/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QUrl>
#include <QStorageInfo>
#include <QListWidgetItem>
#include <QScroller>
#include <QPushButton>
#include <QPluginLoader>
#include <QDirIterator>
#include <QMimeData>
#include <QClipboard>

#include "desktopfile.h"
#include "ioprocesses.h"
#include "variables.h"
#include "appopenfunc.h"
#include "pinmanage.h"
#include "systemxdg.h"
#include "themefunc.h"
#include "filefunc.h"
#include "applicationdialog.h"
#include "trashmanager.h"

#include "shareit.h"
#include "ui_shareit.h"

using namespace CPrime;

ShareIT::ShareIT(const QStringList &files, const QSize &iconSize, QWidget *parent)
	: QDialog(parent)
	, ui(new Ui::ShareIT)
	, mFiles(files)
	, mParent(parent)
	, m_iconSize(iconSize)
{
	ui->setupUi(this);

	resize(500, 600);

	setWindowFlags(Qt::WindowStaysOnTopHint);

	mimeHandler = SystemXdgMime::instance();

	// Uniform Item Size, Grid Size etc
	ui->selectedFiles->setGridSize(QSize(iconSize.width() * 2.2, iconSize.height() * 2.2));

    QScroller::grabGesture(ui->items, QScroller::LeftMouseButtonGesture);
    QScroller::grabGesture(ui->folders, QScroller::LeftMouseButtonGesture);
    QScroller::grabGesture(ui->selectedFiles, QScroller::LeftMouseButtonGesture);

	// Hide the folders slist widget
	ui->folders->hide();

	ui->items->setIconSize(iconSize);
	ui->folders->setIconSize(iconSize);
	ui->selectedFiles->setIconSize(iconSize);
	ui->selectedFiles->setFixedHeight(iconSize.height() * 2.25);
	ui->selectedL->setText(QString::number(files.count()) + " Files Selected");

	// Populate selected files list
	Q_FOREACH (const QString &file, mFiles) {
		QListWidgetItem *item = new QListWidgetItem(CPrime::FileUtils::baseName(file));
		item->setData(Qt::UserRole, Type::Folders);
		item->setIcon(CPrime::ThemeFunc::getFileIcon(file));
		ui->selectedFiles->addItem(item);
	}

	populateActions();
	populatePlugins();
	populateRelatedApps();
	populateFolders();

	connect(ui->items, &QListWidget::itemClicked, this, &ShareIT::itemSelected);
	connect(ui->cancelBtn, &QPushButton::clicked, this, &QDialog::reject);
}

ShareIT::~ShareIT()
{
	Q_FOREACH (ShareItInterface *iface, plugins) {
		if (iface) {
			delete iface;
		}
	}

	delete mimeHandler;
	delete ui;
}

void ShareIT::itemSelected(QListWidgetItem *item)
{
	Type t = item->data(Qt::UserRole).value<Type>();

	qDebug() << "Type " << t;
	qDebug() << "String " << item->data(Qt::UserRole + 1).toString();

	if (t == Type::Plugins) {
		plugin_itemActivated(item);
	} else if (t == Type::Apps) {
		apps_itemActivated(item);
	} else if (t == Type::Folders) {
		folders_itemActivated(item);
	} else if (t == Actions) {
		action_itemActivated(item);
	}
}

void ShareIT::populateActions()
{
	// add copy to clipbaord
	QListWidgetItem *itemcc = new QListWidgetItem("Copy to Clipboard");
	itemcc->setData(Qt::UserRole, Type::Actions);
	itemcc->setData(Qt::UserRole + 1, "clipboard");
	itemcc->setIcon(QIcon::fromTheme("edit-copy"));
	itemcc->setText("Copy to Clipboard");
	ui->items->addItem(itemcc);

	// add copy to clipbaord
	QListWidgetItem *itemol = new QListWidgetItem("Open file location");
	itemol->setData(Qt::UserRole, Type::Actions);
	itemol->setData(Qt::UserRole + 1, "location");
	itemol->setIcon(QIcon::fromTheme("document-open"));
	itemol->setText("Open file location");
	ui->items->addItem(itemol);

	// add open with
	QListWidgetItem *itemow = new QListWidgetItem("Open With");
	itemow->setData(Qt::UserRole, Type::Actions);
	itemow->setData(Qt::UserRole + 1, "open");
	itemow->setIcon(QIcon::fromTheme("document-open"));
	itemow->setText("Open With");
	ui->items->addItem(itemow);

	// add move to trash
	QListWidgetItem *itemmt = new QListWidgetItem("Move to Trash");
	itemmt->setData(Qt::UserRole, Type::Actions);
	itemmt->setData(Qt::UserRole + 1, "trash");
	itemmt->setIcon(QIcon::fromTheme("edit-delete"));
	itemmt->setText("Move to Trash");
	ui->items->addItem(itemmt);
}

void ShareIT::populateRelatedApps()
{
	QStringList selectedApps;

	// Populate selected files possible apps that can open them
	Q_FOREACH (const QString &file, mFiles) {
		qDebug() << file;

		// Load default applications for current item
		AppsList apps = mimeHandler->appsForMimeType(mimeDbInstance.mimeTypeForFile(file));

		Q_FOREACH (const CPrime::DesktopFile &app, apps) {
			if (not selectedApps.contains(app.name())) {
				selectedApps << app.name();
				QListWidgetItem *item = new QListWidgetItem(app.name());
				item->setData(Qt::UserRole, Type::Apps);
				item->setData(Qt::UserRole + 1, QVariant::fromValue<CPrime::DesktopFile>(app));
				item->setIcon(CPrime::ThemeFunc::getAppIcon(app.desktopName()));
				item->setText(app.genericName());
				ui->items->addItem(item);
			}
		}
	}
}

void ShareIT::populatePlugins()
{
	QDir pluginsDir("/usr/lib/coreapps/shareit/");

	Q_FOREACH (const QString &pluginSo, pluginsDir.entryList(QStringList() << "*.so", QDir::Files, QDir::Name | QDir::IgnoreCase)) {
		QPluginLoader loader(pluginsDir.filePath(pluginSo));
		QObject *pObject = loader.instance();

		if (pObject) {
			ShareItInterface *iface = qobject_cast<ShareItInterface *>(pObject);

			/* iface is proper, we add the plugin */
			if (iface) {
				/* Check if the mimetypes match; check only first file */
				/* TODO: FIX this for multiple files later */
				QString firstMime = CPrime::FileUtils::mimeType(mFiles.at(0)).name();

				if (iface->mimeTypes().contains(firstMime) or iface->mimeTypes().contains("*")) {
					QString name = iface->name();
					QIcon icon = iface->icon();
					QListWidgetItem *item = new QListWidgetItem(icon, name, ui->items);
					item->setData(Qt::UserRole, Type::Plugins);
					item->setData(Qt::UserRole + 1, plugins.count());                            // Index of the plugin in the list
					plugins << iface;
				}
			} else {
				qWarning() << "Plugin Error:" << loader.errorString();
			}
		} else {
			qWarning() << "Plugin Error:" << loader.errorString();
		}
	}
}

void ShareIT::populateFolders()
{
	QIcon folder = QIcon::fromTheme("folder");

	// Speed Dial folders
	PinManage pins;
	QStringList pinList = pins.getPinNames("Speed Dial");

	Q_FOREACH (const QString &f, pinList) {
		QString path = pins.pinPath("Speed Dial", f);

		if (QFileInfo(path).isFile()) {
			continue;
		}

		QListWidgetItem *item = new QListWidgetItem(f);
		item->setData(Qt::UserRole, Type::Folders);
		item->setData(Qt::UserRole + 1, pins.pinPath("Speed Dial", f));
		item->setIcon(folder);
		ui->items->addItem(item);
	}

	// Home folders (Desktop, documents, downloads, music, pictures, videos)
	QStringList fList;
	fList << "Desktop" << "Documents" << "Downloads" << "Music" << "Pictures" << "Videos";
	QString homePath = QDir::homePath();

	Q_FOREACH (const QString &str, fList) {
		if (CPrime::FileUtils::isDir(homePath + "/" + str)) {
			QListWidgetItem *item = new QListWidgetItem(str);
			item->setData(Qt::UserRole, Type::Folders);
			item->setData(Qt::UserRole + 1, homePath + "/" + str);
			item->setIcon(folder);
			ui->items->addItem(item);
		}
	}

	// Drives
	Q_FOREACH (const QStorageInfo &info, QStorageInfo::mountedVolumes()) {
		if (info.device() == QByteArray("tmpfs")) {
			continue;
		}

		QListWidgetItem *item = new QListWidgetItem(info.displayName());
		item->setData(Qt::UserRole, Type::Folders);
		item->setData(Qt::UserRole + 1, info.rootPath());
		item->setIcon(QIcon::fromTheme("drive-harddisk"));
		ui->items->addItem(item);
	}
}

void ShareIT::folders_itemActivated(QListWidgetItem *item)
{
	QString driveFolder = toDriveFolder(item->data(Qt::UserRole + 1).toString());
	qDebug() << driveFolder;

	if (not driveFolder.isEmpty()) {
		CPrime::IOProcess *process = new CPrime::IOProcess;
		process->sourceDir = CPrime::FileUtils::dirName(mFiles.at(0));
		process->targetDir = driveFolder;

		if (not process->sourceDir.endsWith("/")) {
			process->sourceDir += "/";
		}

		if (not process->targetDir.endsWith("/")) {
			process->targetDir += "/";
		}

		QStringList srcList;

		for (QString file : mFiles) {
			if (CPrime::FileUtils::exists(file)) {
				srcList << file.replace(process->sourceDir, "");
			}
		}

		if (not srcList.count()) {
			return;
		}

		process->type = CPrime::Copy;

		IODialog *pasteDlg = new IODialog(srcList, process);
		pasteDlg->show();
	}
}

void ShareIT::apps_itemActivated(QListWidgetItem *item)
{
	CPrime::DesktopFile app = item->data(Qt::UserRole + 1).value<CPrime::DesktopFile>();
	app.startApplicationWithArgs(mFiles);

	this->close();
}

void ShareIT::plugin_itemActivated(QListWidgetItem *item)
{
	int index = item->data(Qt::UserRole + 1).toInt();
	plugins.at(index)->shareItDialog(mFiles, mParent);
	close();
}

void ShareIT::action_itemActivated(QListWidgetItem *item)
{
	if (item->data(Qt::UserRole + 1).toString() == "clipboard") {
		copyToClipboard(item);
		return;
	}

	if (item->data(Qt::UserRole + 1).toString() == "location") {
        openFileLocation();
		return;
	}


	if (item->data(Qt::UserRole + 1).toString() == "open") {
		openFileWith();
		return;
	}

	if (item->data(Qt::UserRole + 1).toString() == "trash") {
		moveToTrash();
		return;
	}

	close();
}

QString ShareIT::toDriveFolder(QString driveMountPoint)
{
	ui->items->hide();
	ui->selectedFiles->hide();
	ui->title->hide();
	ui->selectedL->setText("Select a folder to copy");
	ui->folders->show();

	QListWidgetItem *item = nullptr;
	QIcon folder = QIcon::fromTheme("folder");
	QDirIterator iterator(driveMountPoint, QDir::Dirs | QDir::NoDotDot);

	while (iterator.hasNext()) {
		QString temp = iterator.next();
		item = new QListWidgetItem(temp);
		item->setData(Qt::UserRole + 1, temp);
		item->setIcon(folder);
		ui->folders->addItem(item);
	}

	connect(ui->folders, &QListWidget::itemClicked, [ = ](QListWidgetItem * item) {
		shareSelection = item->text();
		accept();
	});

	/* Wait Loop for the user to make selection */
	QEventLoop *loop = new QEventLoop(this);
	connect(this, &QDialog::finished, loop, &QEventLoop::quit);

	loop->exec();

	return shareSelection;
}

void ShareIT::copyToClipboard(QListWidgetItem *item)
{
	Q_UNUSED(item)

	if (mFiles.count() == 1) {
		qDebug() << "Found One File...";
		QMimeDatabase mimeDbInstance;
		QString mtype = mimeDbInstance.mimeTypeForFile(mFiles.at(0)).name();
		mtype.resize(5);

		if (mtype == "image") {
			qDebug() << "Mime type image";
			QApplication::clipboard()->setPixmap(QPixmap(mFiles.at(0)));
			qDebug() << "Coping to clipboard";
			this->close();
			return;
		}
	}

	QList<QUrl> urlList;

	Q_FOREACH (const QString &item, mFiles) {
		urlList << QUrl::fromLocalFile(item);
	}

	QClipboard *clipboard = QApplication::clipboard();
	QMimeData *data = new QMimeData();

	QByteArray uriList;

	for (QUrl url : urlList) {
		uriList += url.toEncoded();
		uriList += "\r\n";
	}

	// Add current pid to trace cut/copy operations to current app
	// data->setData(QStringLiteral("text/x-libfmqt-pid"), ba.setNum(QCoreApplication::applicationPid()));
	// Gnome, LXDE, and XFCE
	// Note: the standard text/urilist format uses CRLF for line breaks, but gnome format uses LF only
	data->setData("x-special/gnome-copied-files", QByteArray("copy\n") + uriList.replace("\r\n", "\n"));
	// The KDE way
	data->setData("text/uri-list", uriList);
	// data->setData(QStringLiteral("application/x-kde-cutselection"), QByteArrayLiteral("0"));
	clipboard->setMimeData(data);

	this->close();
}

void ShareIT::openFileLocation()
{
	if (!mFiles.count()) {
		qDebug() << "No file"; // Just for checking
		return;
	}

    CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::FileManager,  CPrime::FileUtils::dirName(mFiles.at(0)), "");
	this->close();
}

void ShareIT::openFileWith()
{
	if (!mFiles.count()) {
		qDebug() << "No file"; // Just for checking
		return;
	}

	ApplicationDialog *dialog = new ApplicationDialog(m_iconSize, nullptr);
	dialog->setFixedSize(this->size());
	QString appName;

	if (dialog->exec()) {
		if (dialog->getCurrentLauncher().compare("") != 0) {
			appName = dialog->getCurrentLauncher();

			qDebug() << "file" << mFiles;
			CPrime::AppOpenFunc::systemAppOpener(appName, mFiles);
		}
	} else {
		qDebug() << "No app selected!!!";
		return;
	}

	this->close();
}

void ShareIT::moveToTrash()
{
	if (!mFiles.count()) {
		qDebug() << "No file"; // Just for checking
		return;
	}

	CPrime::TrashManager::moveToTrash(mFiles);

	this->close();
}
