/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include <QIcon>
#include <QMessageBox>

#include "libcprime_global.h"


namespace CPrime {

class LIBCPRIMESHARED_EXPORT TrashManager {

public:
    static QString trashLocation(const QString &path);
    static bool moveToTrash(const QStringList &filePaths);
    static void restoreFromTrash(const QStringList &filePaths);
    static bool deleteFileTotally(const QStringList &filePaths, bool fromTrash);
    static QString homePartition;

private:
	static int showMessage(QMessageBox::Icon icon, const QString &title,
						   const QString &message, QMessageBox::StandardButtons buttons);
};

}
