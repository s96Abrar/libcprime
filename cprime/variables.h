/*
    *
    * This file is a part of Libcprime
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include "libcprime_global.h"

// Returns coreaction plugin interface name
#define WIDGETS_PLUGININTERFACE "org.cprime.widgetsinterface"

// Returns shareit plugin interface name
#define SHAREIT_PLUGININTERFACE "org.cprime.shareitinterface"

namespace CPrime {

class LIBCPRIMESHARED_EXPORT Variables {

public:
    // Returns the home trash folder path
    static QString CC_Home_TrashDir();

    // Returns the defaultapplist file path
    static QString CC_DefaultAppListFilePath();

    // Returns the system config folder path
    static QString CC_System_ConfigDir();

    // Returns the library config folder path
    static QString CC_Library_ConfigDir();

    // Returns the widgets config folder path
    static QString CC_Widgets_ConfigDir();

    // Returns the Pins file name
    static QString CC_PinsFile() {
        return "pins";
    }

    // Returns the Pins file path
    static QString CC_PinsFilePath();

    // Returns the Recent activity file
    static QString CC_ActivitesFile() {
        return "activities";
    }

    // Returns the Recent activity file path
    static QString CC_ActivitiesFilePath();

    // Returns the DefaultAppList file name
    static QString CC_DefaultAppListFile() {
        return "defaultapplist.list";
    }
};

}
