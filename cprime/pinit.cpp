/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QTimer>
#include <QFileInfo>
#include <QIcon>
#include <QInputDialog>
#include <QMessageBox>

#include "filefunc.h"
#include "themefunc.h"
#include "messageengine.h"

#include "pinit.h"
#include "ui_pinit.h"


PinIT::PinIT(const QStringList &files, QWidget *parent) : QDialog( parent )
  , ui(new Ui::PinIT)
  , mFiles(files)
{
    ui->setupUi(this);

    setWindowIcon(QIcon::fromTheme("bookmark-new"));

    // set the requried folders
    CPrime::FileUtils::setupFolder(CPrime::FolderSetup::ConfigFolder);

	connect(ui->cancel, &QToolButton::clicked, this, &PinIT::reject);

    ui->pinSection->clear();
    ui->pinSection->addItems(pin.getPinSections());
    ui->done->setEnabled(false);

    QFileInfo info(mFiles.at(0));
    const QString str = pin.checkingPinPathEx(mFiles.at(0));

    if (str.count() == 0) {
        QIcon ico = CPrime::ThemeFunc::getFileIcon(mFiles.at(0));
        QPixmap pix = ico.pixmap(QSize(100, 80));
        setPinPath(mFiles.at(0));
        setPinName(info.fileName() + "");
        checkPath();
    } else {
		CPrime::MessageEngine::messageEngine("bookmark-new", "PinIT", "Can't pin selected file(s)", str);
		ui->pinName->setEnabled(false);
		ui->pinStatus->setText(str);
		ui->path->setText("");
    }
}

PinIT::~PinIT()
{
    delete ui;
}

void PinIT::on_done_clicked()
{
    if (ui->pinName->text().count() == 0) {
        ui->done->setEnabled(false);
    }

    if (ui->pinName->text().count() != 0 && ui->pinSection->currentText().count() != 0) {
        pin.addPin(getSectionName(), getPinName(), mFiles.at(0));
        QTimer::singleShot(100, this, SLOT(close()));
        // Function from utilities.cpp
        qDebug()<< "Pin Added at '" + ui->pinSection->currentText();
		accept();
    }
}

void PinIT::pinName_Changed()
{
    if (ui->pinName->text().count() > 0) {
        QString str = pin.checkingPinName(ui->pinSection->currentText(), ui->pinName->text());

        if (str.count() > 0) {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(false);
        } else {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(true);
        }
    } else {
        ui->done->setEnabled(false);
    }
}

void PinIT::checkPath()
{
    QString str = pin.checkingPinPath(ui->pinSection->currentText(), ui->pinName->text());

    if (str.count() > 0) {
        ui->pinStatus->setText(str);
        ui->pinName->setEnabled(false);
        ui->done->setEnabled(false);
        ui->cancel->setText("OK");
    } else {
        ui->pinStatus->setText(str);
        ui->pinName->setEnabled(true);
        ui->done->setEnabled(true);
        ui->cancel->setText("Cancel");
    }
}

void PinIT::setPinPath(const QString &path)
{
    ui->path->setText(path);
}

void PinIT::setPinName(const QString &bName)
{
    ui->pinName->setText(bName);
}

QString PinIT::getPinName()
{
    return ui->pinName->text();
}

QString PinIT::getSectionName()
{
    return ui->pinSection->currentText();
}

void PinIT::item_Changed()
{
    checkPath();
    pinName_Changed();
}

void PinIT::on_pinName_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1)

    if (ui->pinName->text().count() > 0) {
        QString str = pin.checkingPinName(ui->pinSection->currentText(), ui->pinName->text());

        if (str.count() > 0) {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(false);
        } else {
            ui->pinStatus->setText(str);
            ui->done->setEnabled(true);
        }
    } else {
        ui->done->setEnabled(false);
    }
}

void PinIT::on_pinSection_currentIndexChanged(const QString &arg1)
{
	ui->deleteSection->setDisabled(arg1 == "Speed Dial");

    checkPath();
    pinName_Changed();
}

void PinIT::on_deleteSection_clicked()
{
	QString currentSection = ui->pinSection->currentText();
	if (currentSection == "Speed Dial")
		return;

	int exec = QMessageBox::question(this, "Delete Section",
									 QString("Do you want to delete this section '%1'").arg(currentSection),
									 QMessageBox::No | QMessageBox::Yes);

	if (exec == QMessageBox::Yes) {
		pin.delSection(currentSection);
		ui->pinSection->removeItem(ui->pinSection->currentIndex());
	}
}

void PinIT::on_addSection_clicked()
{
	QString sectionName;
	bool sectionExists = false;
	bool ok;
	bool tryAgain = false;
	do {
		sectionName = QInputDialog::getText(this, "Create New Section", "Enter new section name: ", QLineEdit::Normal, "", &ok);

		if (ok) {
			if (pin.getPinSections().contains(sectionName, Qt::CaseInsensitive)) {
				sectionExists = true;
				int exec = QMessageBox::question(this, "Section Exists",
												 "Given Section Exists.\nDo you want to enter another section?",
												 QMessageBox::Yes | QMessageBox::No);
				if (exec == QMessageBox::Yes)
					tryAgain = true;
				else
					tryAgain = false;
			} else {
				sectionExists = false;
				tryAgain = false;
			}
		}

	} while (tryAgain);

	if (ok && !sectionExists) {
		pin.addSection(sectionName);
		ui->pinSection->addItem(sectionName);
	}
}
