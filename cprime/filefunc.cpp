/*
    *
    * This file is a part of Libcprime
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QFile>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>

#include <dirent.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <sys/statfs.h>
#include <unistd.h>

#include "filefunc.h"
#include "variables.h"


QString CPrime::FileUtils::formatSize(const quint64 &size)
{
	if ( size >= TB )
		return QString( "%1 TiB" ).arg( QString::number( qreal( size ) / TB, 'f', 2 ) );

	else if ( size >= GB )
		return QString( "%1 GiB" ).arg( QString::number( qreal( size ) / GB, 'f', 2 ) );

	else if ( size >= MB )
		return QString( "%1 MiB" ).arg( QString::number( qreal( size ) / MB, 'f', 2 ) );

	else if ( size >= KB )
		return QString( "%1 KiB" ).arg( QString::number( qreal( size ) / KB, 'f', 2 ) );

	else
		return QString( "%1 B" ).arg( size );
}

double CPrime::FileUtils::formatSizeRaw( const quint64 &size )
{
	if ( size >= TB )
		return double( size ) / TB;

	else if ( size >= GB )
		return double( size ) / GB;

	else if ( size >= MB )
		return double( size ) / MB;

	else if ( size >= KB )
		return double( size ) / KB;

	else
		return double( size );
}

QString CPrime::FileUtils::formatSizeStr( const quint64 &size )
{
	if ( size >= TB )
		return QString( "TiB" );

	else if ( size >= GB )
		return QString( "GiB" );

	else if ( size >= MB )
		return QString( "MiB" );

	else if ( size >= KB )
		return QString( "KiB" );

	else
		return QString( "B" );
}

quint64 CPrime::FileUtils::maxYForSize( quint64 fileSize ) {

	QList<quint64> sizes = { 1ULL << 0, 1ULL << 10, 1ULL << 20, 1ULL << 30, 1ULL << 40 };
    QList<quint64> widths = { 1, 2, 5, 10, 20, 50, 100, 200, 500 };

	Q_FOREACH( quint64 size, sizes ) {
        Q_FOREACH( quint64 width, widths ) {
			if ( fileSize < width * size )
				return width * size;
		}
	}

	return 0;
}

quint64 CPrime::FileUtils::getFileSize( const QString &path )
{
	struct stat statbuf;
	if ( stat( path.toLocal8Bit().constData(), &statbuf ) != 0 )
		return 0;

	switch( statbuf.st_mode & S_IFMT ) {
		case S_IFREG: {

			return statbuf.st_size;
		}

		case S_IFDIR: {
			DIR* d_fh;
			struct dirent* entry;
			QString longest_name;

            while ( ( d_fh = opendir( path.toLocal8Bit().constData() ) ) == nullptr ) {
				qWarning() << "Couldn't open directory:" << path;
				return statbuf.st_size;
			}

			quint64 size = statbuf.st_size;

			longest_name = QString( path );
			if ( not longest_name.endsWith( "/" ) )
				longest_name += "/";

            while( ( entry = readdir( d_fh ) ) != nullptr ) {

				/* Don't descend up the tree or include the current directory */
				if ( strcmp( entry->d_name, ".." ) != 0 && strcmp( entry->d_name, "." ) != 0 ) {

					if ( entry->d_type == DT_DIR ) {

						/* Recurse into that folder */
						size += getFileSize( longest_name + entry->d_name );
					}

					else {

						/* Get the size of the current file */
						size += getFileSize( longest_name + entry->d_name );
					}
				}
			}

			closedir( d_fh );
			return size;
		}

		default: {

			/* Return 0 for all other nodes: chr, blk, lnk, symlink etc */
			return 0;
		}
	}

	/* Should never come till here */
	return 0;
}

QString CPrime::FileUtils::dirName( QString path ) {

	if ( path == "/" or path == "//" )
		return "/";

	/* Simple clean path: remove '//' and './' */
	path = path.replace( "//", "/" ).replace( "/./", "/" );

	char *dupPath = strdup( path.toLocal8Bit().constData() );
	QString dirPth = QString::fromLocal8Bit( dirname( dupPath ) );
	dirPth += ( dirPth.endsWith( "/" ) ? "" : "/" );
	free( dupPath );

	return dirPth;
}

QString CPrime::FileUtils::baseName(QString path ) {

	if ( path == "/" or path == "//" )
		return "/";

	/* Simple clean path" remove '//' and './' */
	path = path.replace( "//", "/" ).replace( "/./", "/" );

	char *dupPath = strdup( path.toLocal8Bit().constData() );
	QString basePth = QString::fromLocal8Bit( basename( dupPath ) );
	free( dupPath );

	return basePth;
}

int CPrime::FileUtils::mkpath(QString path, mode_t mode ) {

	/* Root always exists */
	if ( path == "/" )
		return 0;

	/* If the directory exists, thats okay for us */
	if ( exists( path ) )
		return 0;

	mkpath( dirName( path ), mode );

	return mkdir( path.toLocal8Bit().constData(), mode );
}

bool CPrime::FileUtils::isFile( const QString &path ) {

	struct stat statbuf;
	if ( stat( path.toLocal8Bit().constData(), &statbuf ) == 0 )

		if ( S_ISREG( statbuf.st_mode ) or S_ISLNK( statbuf.st_mode ) )
			return true;

		else
			return false;

	else
		return false;
}

bool CPrime::FileUtils::isDir( const QString &path ) {

	struct stat statbuf;
	if ( stat( path.toLocal8Bit().constData(), &statbuf ) == 0 )

		if ( S_ISDIR( statbuf.st_mode ) )
			return true;

		else
			return false;

	else
		return false;
}

bool CPrime::FileUtils::isLink(const QString &path ) {

	struct stat statbuf;
	if ( lstat( path.toLocal8Bit().constData(), &statbuf ) == 0 )
		if ( S_ISLNK( statbuf.st_mode ) )
			return true;

		else
			return false;

	else
		return false;
}

bool CPrime::FileUtils::exists( const QString &path ) {

	return not access( path.toLocal8Bit().constData(), F_OK );
}

bool CPrime::FileUtils::removeDir(const QString &dirName ) {

	bool result = true;
	QDir dir( dirName);

	if ( dir.exists( dirName ) ) {
		Q_FOREACH(const QFileInfo &info, dir.entryInfoList( QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, QDir::DirsFirst ) ) {
			if ( info.isDir() )
				result = removeDir( info.absoluteFilePath() );

			else
				result = QFile::remove( info.absoluteFilePath() );

			if ( !result )
				return result;
		}
		result = dir.rmdir( dirName );
	}

	return result;
}

bool CPrime::FileUtils::isReadable( const QString &path ) {

	if ( isDir( path ) )
		return not access( path.toLocal8Bit().constData(), R_OK | X_OK );

	else
		return not access( path.toLocal8Bit().constData(), R_OK );
}

bool CPrime::FileUtils::isWritable( const QString &path ) {

	if ( isDir( path ) )
		return not access( path.toLocal8Bit().constData(), W_OK | X_OK );

	else
		return not access( path.toLocal8Bit().constData(), W_OK );
}

QString CPrime::FileUtils::readLink( const QString &path ) {

	char linkTarget[ PATH_MAX ] = { 0 };
	realpath( path.toLocal8Bit().constData(), linkTarget );

	return QString( linkTarget );
}

QMimeType CPrime::FileUtils::mimeType( const QString &path ) {

	return mimeDbInstance.mimeTypeForFile( path );
}

bool CPrime::FileUtils::setupFolder(CPrime::FolderSetup fs)
{
    bool status = false;

    switch (fs) {
        case CPrime::ConfigFolder: {
            // If the permissions of ConfigFolder are right
            QFileInfo assume( CPrime::Variables::CC_Library_ConfigDir() );

            if ( assume.exists() && assume.permission( QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner ) ) {
                status = true;
            } else {
                QString configDir = CPrime::Variables::CC_Library_ConfigDir();

                // Try to make the ConfigFolder
                try {
                    QDir::home().mkpath( configDir );
                    QFile::setPermissions( configDir, QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );
                } catch ( ... ) {
                    qDebug() << "Error " << errno << ": func( homeConfigLocation ) : " << strerror( errno );
                }

                qDebug() << "whats this " << configDir;
            }

            break;
        }

        case CPrime::SystemTrashFolder: {
            /* If the permissions of Trash folder are right; we assume $trash/files and $trash/info exists */
            QFileInfo assume( CPrime::Variables::CC_Home_TrashDir() );

            if ( assume.exists() && assume.permission( QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner ) ) {
                status = true;
            } else {
                QString trashDir = CPrime::Variables::CC_Home_TrashDir();

                // Try to make the trash folder
                try {
                    QDir::home().mkpath( trashDir );
                    QDir::home().mkpath( trashDir + "files/" );
                    QDir::home().mkpath( trashDir + "info/" );
                    QFile::setPermissions( trashDir, QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );
                } catch ( ... ) {
                    qDebug() << "Error " << errno << ": func( homeTrashLocation ) : " << strerror( errno );
                }

                qDebug() << "whats this " << trashDir;
            }

            break;
        }

        case CPrime::WidgetConfigFolder: {
            // If the permissions of ConfigFolder are right
            QFileInfo assume( CPrime::Variables::CC_Widgets_ConfigDir() );

            if ( assume.exists() && assume.permission( QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner ) ) {
                status = true;
            } else {
                QString configDir = CPrime::Variables::CC_Library_ConfigDir();

                // Try to make the ConfigFolder
                try {
                    QDir::home().mkpath( configDir );
                    QFile::setPermissions( configDir, QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );
                } catch ( ... ) {
                    qDebug() << "Error " << errno << ": func( widgetsConfigLocation ) : " << strerror( errno );
                }

                qDebug() << "whats this " << configDir;
        }

        break;
    }

        default:
            break;
    }

    return status;
}
