/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include "libcprime_global.h"


class QSettings;

class LIBCPRIMESHARED_EXPORT PinManage
{

public:
	explicit PinManage();
	~PinManage();

	void addSection(const QString &sectionName);
	void addPin(const QString &sectionName, const QString &pinName, const QString &pinpath);

	QStringList getPinSections();
	QStringList getPinNames(const QString &sectionName);
	QString piningTime(const QString &sectionName, const QString &pinName);
	QString pinPath(const QString &sectionName, const QString &pinName);
	QString checkingPinName(const QString &section, const QString &pinName);
	QString checkingPinPath(const QString &section, const QString &pinPath);
	QString checkingPinPathEx(const QString &pinpath);

	void delSection(const QString &sectionName);
	void changeAll(const QString &oldSectionName, const QString &oldpinName,
				   const QString &sectionName, const QString &pinName, const QString &pinValue);
	void editPin(const QString &sectionName, const QString &pinName, const QString &pinpath);
	void delPin(const QString &pinName, const QString &section);

private:
	QSettings *pinSettings;
	QString pinValues(const QString &sectionName, const QString &pinName) const;
};
