/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QDialog>

#include "pinmanage.h"
#include "libcprime_global.h"


namespace Ui {
    class PinIT;
}

class LIBCPRIMESHARED_EXPORT PinIT : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Call add pinit dialog from any apps by giving the path to pin it.
     * @brief If a stringlist is sent, only first file will be pinned.
     * @param files : Path which needs to be pinned.
     */
    explicit PinIT(const QStringList &files = QStringList(), QWidget *parent = nullptr);
    ~PinIT();


private slots:
    void on_done_clicked();
    void pinName_Changed();
    void item_Changed();
    void on_pinName_textChanged(const QString &arg1);
    void on_pinSection_currentIndexChanged(const QString &arg1);

	void on_deleteSection_clicked();

	void on_addSection_clicked();

private:
    Ui::PinIT *ui;
    PinManage pin;

    QStringList mFiles;

    void checkPath();
    void setPinPath(const QString &path);
    void setPinName(const QString &bName);

    QString getPinName();
    QString getSectionName();
};
